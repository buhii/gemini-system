# -*- coding: utf-8 -*-
"""
検出器の設定
"""
import os.path
LOCAL_OPERATION_DIR = os.path.join(os.path.dirname(__file__), "../operations/")


CAMERA_SIZE = (640, 480)

RSSI_THRESHOLD = -90
RSSI_BRO_THRESHOLD = -90
RSSI_ALONE = -70
RSSI_MIN = -128

# sentinels and force-direction dictionary
SENTINELS_COMPASS = {
    '0x1009': 120,
    '0x1006': 305,
    '0x1003': 120,
    '0x1005': 305
}

WORKS_COMPASS = {
    '0x1004': 180,
    '0x1002': 70,
    '0x1007': 70,
}


ROLE_WAIT = 'GREEN'


HEAD_ICS = {
    'pitch': {
        'RED': 1,
        'GREEN': 1,
        },
    'yaw': {
        'RED': 0,
        'GREEN': 0,
        },
}
