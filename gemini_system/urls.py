from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'gemini_system.views.home', name='home'),
    # url(r'^gemini_system/', include('gemini_system.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^museum/servomotion/add_by_xml/$', 'views.admin.add_by_xml', name="admin_add_by_xml"),
    url(r'^', include(admin.site.urls)),
)

if settings.DEBUG:
   urlpatterns += staticfiles_urlpatterns()
