import time
from threading import Thread
from pyliburg import urg_wrapper as UrgDevice
from omniwheel import F, B, L, R

u = UrgDevice('/dev/ttyACM0')
min_distance = u.minDistance()

class HalfRangeUrg(Thread):
    def __init__(self):
        Thread.__init__(self)

    def run(self):
        for i in range(300):
            t = u.capture()
            data = filter(lambda d: d > min_distance, t)
            print min(data), max(data), len(data)

HalfRangeUrg().start()
F(2)
time.sleep(10)
B(2)
time.sleep(10)
L(120)
time.sleep(5)
R(120)
time.sleep(5)

