# -*- coding: utf-8 -*-
import sys, time, os
from datetime import datetime
from random import randint
from math import radians, sin, cos
import pygame
from pygame.locals import *
from cStringIO import StringIO
from utils.colors import *
from utils.devices import Me
from utils.scenario import ExhibitionYaml
from gemini_system.universal import RSSI_THRESHOLD, RSSI_BRO_THRESHOLD, LOCAL_OPERATION_DIR
from utils.afsm import AFSM
from multiprocessing import Process, Manager
from sandbox.urg_sample import urg_samples
from utils.commons import calc_front_wall_for_draw, polar2cartesian as p2c, decompress_urg_data
from utils.kondo import FaceMotion
from twisted.internet.task import LoopingCall
from txosc import osc
from record.models import ConversationData


exhibition_yaml = ExhibitionYaml(pk=2)
is_record = False


CAMERAMAN_TIMESPAN = 17.0

# action_end flag
manager = Manager()
shared_memory = manager.dict(
    action_end=False,
    reliable_stuff=None,
    compass_head=0,
    urg_green=[],
    urg_red=[],
    is_playing_nearby=False,
    is_playing=False,
    stop_playing=False,
    remm_work_pk=0,
    remm_return_pk=0,
    cameraman_id=9999,
    cameraman_last_play=time.time() - CAMERAMAN_TIMESPAN,
)


# FIXME: code clone! (@omniwheel.py)
X = AFSM.STOP
L = AFSM.DIRECTION_L
R = AFSM.DIRECTION_R
F = AFSM.DIRECTION_F
B = AFSM.DIRECTION_B
C = AFSM.TURN_TO

def get_local_file(path):
    return os.path.normpath(os.path.join(LOCAL_OPERATION_DIR, path))


def gemini_moving(role, key, is_start):
    if role == 'RED':
        to = Me.red
    elif role == 'GREEN':
        to = Me.green
    direction = X
    if is_start:
        if key in (K_a, K_LEFT):
            direction = L
        elif key in (K_w, K_UP):
            direction = F
        elif key in (K_d, K_RIGHT):
            direction = R
        elif key in (K_s, K_DOWN):
            direction = B
    msg = osc.Message('/move')
    msg.add(direction)
    print "[MOVE] %s: direction %s" % (role, direction)
    Me.send(to, msg)


def play_action(action, role, path, _time=None, reliable_stuff=None):
    msg = osc.Message(action)
    if _time:
        msg.add(_time)
    else:
        msg.add(0.0)
    msg.add(str(path))
    if reliable_stuff:
        msg.add(reliable_stuff)

    result = Me.send(role, msg)

OMNI_DIRECTIONS = {
    'L': AFSM.DIRECTION_L,
    'R': AFSM.DIRECTION_R,
    'F': AFSM.DIRECTION_F,
    'B': AFSM.DIRECTION_B,
    'X': AFSM.STOP,
    'C': AFSM.TURN_TO
}


def turn_to():
    from front import FRONT_ROTATE
    msg = osc.Message('/rotate_direction')
    msg.add(FRONT_ROTATE)
    print "[ROTATE_DIRECTION] TURN_TO: %s" % FRONT_ROTATE
    Me.send(Me.children, msg)


def process_content(content_excel, _shared_memory, actions):
    # stage a play
    _shared_memory['is_playing'] = True

    for action in actions:
        # sleep for conversation span
        time.sleep(0.1)

        if _shared_memory['stop_playing']:
            # stop a play
            _shared_memory['stop_playing'] = False
            _shared_memory['is_playing'] = False
            break

        print action

        # get old action_end time
        old_action_end = _shared_memory['action_end']
        # find which role
        if action['performer'] == u'B':
            to = Me.red
        elif action['performer'] == u'A':
            to = Me.green
        else:
            raise ValueError('invalid performer! %s' % action['performer'])

        # get time
        try:
            time_span = action['timespan']
        except:
            time_span = 0.0
        try:
            omni_timespan = action['omni_timespan']
        except:
            omni_timespan = None
        if omni_timespan and time_span < omni_timespan:   # set time_span more longer
            time_span = omni_timespan

        # sound path
        sound_path = action['sound']
        reliable_stuff = randint(-2147483648, 2147483647)  # generate reliable stuff

        if sound_path:
            play_action('/voice', to, sound_path, time_span, reliable_stuff)
        else:
            pass

        # motion(servo)
        motion = action['servo__code']

        if motion:
            play_action('/servo', to, motion, time_span)

        # omniwheel
        omni_t = action['omni_timespan']    # float
        omni_d = action['omni_direction']   # L, R, F, B, X, C
        if omni_t and omni_d:
            print "[OMNI]", omni_d, omni_t
            print to
            play_action('/omni', to, OMNI_DIRECTIONS[omni_d], omni_t)

        if not sound_path:
            # sleep
            time.sleep(time_span)
            continue

        # wait until action_end value changes
        while old_action_end == shared_memory['action_end'] and \
                reliable_stuff != shared_memory['reliable_stuff']:
            time.sleep(0.01)   # very quickly
        print shared_memory['action_end']

    # end a play
    _shared_memory['is_playing'] = False
    # turn to compass direction
    turn_to()


def play_remm():
    remm_pk = shared_memory['remm_work_pk']
    if remm_pk == 0:
        shared_memory['remm_work_pk'] = shared_memory['remm_return_pk'] + 1
        remm_pk = 1
    actions = []

    work = exhibition_yaml[remm_pk]   # No.0 is camera.
    print work['name']
    actions.extend(work['actions'])

    # update remm pk
    remm_pk = (remm_pk + 1) % 5
    shared_memory['remm_work_pk'] = remm_pk

    p = Process(
        target=process_content,
        args=(1, shared_memory, actions))
    try:
        p.start()
    except KeyboardInterrupt:
        p.terminate()


def play_content(key):
    work_pk = {
        K_h: 0,
        K_j: 1,
        K_k: 2,
        K_l: 3,
        K_SEMICOLON: 4,
        }
    print "[INFO] play content! @ %s" % datetime.now()
    work = exhibition_yaml[work_pk[key]]
    p = Process(
        target=process_content,
        args=(1, shared_memory, work['actions']))
    try:
        p.start()
    except KeyboardInterrupt:
        p.terminate()


def pygame_loop(start_time, is_record, _shared_memory):
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == KEYDOWN and event.key == K_r:
            # record start/stop settings
            if is_record == True:
                is_record = False
                msg = osc.Message('/record_stop')
                print "[INFO] Recording Stopped!!!"
            else:
                is_record = True
                msg = osc.Message('/record_start')
                print "[INFO] Recording Start!!!"
            Me.send(Me.children, msg)

        elif event.type == KEYDOWN and event.key == K_v:
            # continue wandering
            msg = osc.Message('/wander')
            Me.send(Me.children, msg)
            print "sent /wander"

        # -- face motion --
        # up and down...
        elif event.type == KEYDOWN and event.key == K_i:
            print "[INFO] green face down motion!"
            msg = osc.Message('/facemotion')
            msg.add(FaceMotion.DOWN)
            Me.send(Me.green, msg)

        elif event.type == KEYDOWN and event.key == K_u:
            print "[INFO] green face up motion!"
            msg = osc.Message('/facemotion')
            msg.add(FaceMotion.UP)
            Me.send(Me.green, msg)

        elif event.type == KEYDOWN and event.key == K_o:
            print "[INFO] red face down motion!"
            msg = osc.Message('/facemotion')
            msg.add(FaceMotion.DOWN)
            Me.send(Me.red, msg)

        elif event.type == KEYDOWN and event.key == K_p:
            print "[INFO] red face up motion!"
            msg = osc.Message('/facemotion')
            msg.add(FaceMotion.UP)
            Me.send(Me.red, msg)
        # left and right...
        elif event.type == KEYDOWN and event.key == K_7:
            print "[INFO] green face left motion!"
            msg = osc.Message('/facemotion')
            msg.add(FaceMotion.LEFT)
            Me.send(Me.green, msg)

        elif event.type == KEYDOWN and event.key == K_8:
            print "[INFO] green face up motion!"
            msg = osc.Message('/facemotion')
            msg.add(FaceMotion.RIGHT)
            Me.send(Me.green, msg)

        elif event.type == KEYDOWN and event.key == K_9:
            print "[INFO] red face down motion!"
            msg = osc.Message('/facemotion')
            msg.add(FaceMotion.LEFT)
            Me.send(Me.red, msg)

        elif event.type == KEYDOWN and event.key == K_0:
            print "[INFO] red face up motion!"
            msg = osc.Message('/facemotion')
            msg.add(FaceMotion.RIGHT)
            Me.send(Me.red, msg)

        elif event.type == KEYDOWN and event.key in (K_h, K_j, K_k, K_l, K_SEMICOLON):
            play_content(event.key)

        elif (event.type == KEYDOWN or event.type == KEYUP) and event.key in (K_a, K_w, K_d, K_s):
            gemini_moving('GREEN', event.key, event.type==KEYDOWN)

        elif (event.type == KEYDOWN or event.type == KEYUP) and event.key in (K_UP,K_LEFT, K_RIGHT, K_DOWN):
            gemini_moving('RED', event.key, event.type==KEYDOWN)

        elif event.type == KEYDOWN and event.key == K_t:
            # get temperature
            msg = osc.Message('/temperature')
            Me.send(Me.children, msg)

        elif event.type == KEYDOWN and K_1 <= event.key <= K_3:
            # send tracking xbee id
            msg = osc.Message('/track_xbee')
            work_index = event.key - K_0 - 1  # pk ではない
            work = exhibition_yaml[work_index]
            msg.add(str('0x' + work['beacon__source_addr']))
            Me.send(Me.children, msg)
            # send tracking work id
            msg = osc.Message('/track_work')
            msg.add(int(work['pk']))
            print msg
            Me.send(Me.children, msg)
        elif event.type == KEYDOWN and event.key == K_SPACE:
            # restart
            msg = osc.Message('/restart')
            Me.send(Me.children, msg)

        elif event.type == KEYDOWN and event.key == K_n:
            old_is_playing_nearby = shared_memory['is_playing_nearby']
            if old_is_playing_nearby:
                shared_memory['is_playing_nearby'] = False
                print "[INFO] playing nearby disabled."
            else:
                shared_memory['is_playing_nearby'] = True
                print "[INFO] playing nearby enabled."

        elif event.type == KEYDOWN and event.key in (K_z, K_ESCAPE):
            # emergency stop
            msg = osc.Message('/stop')
            Me.send(Me.children, msg)

            # stop a play
            if shared_memory['is_playing']:
                shared_memory['stop_playing'] = True

    # update elapsed time
    draw_elapsed_time(time.time() - start_time, is_record)
    pygame.display.update()

    # Please send ACK!!
    """
    msg = osc.Message('/ack')
    msg.add('ack!')
    Me.send(Me.green, msg)
    """
    #return is_record


IMG_W, IMG_H = 256, 192
MARGIN = 10
width, height = 640, MARGIN * 3 + 256 * 2 + 150
Y_ROBOT_NAME = 10
Y_IMAGE = 42
Y_COMPASS = IMG_H + Y_IMAGE + MARGIN * 2 + IMG_H / 2
Y_RSSI = Y_COMPASS + IMG_H / 2
X_FONT = IMG_W * 1.51 + MARGIN * 2
get_x = lambda role: MARGIN if role == 'GREEN' else MARGIN * 2 + IMG_W + 48
get_color = lambda role: GREEN if role == 'GREEN' else RED
def draw_camera_image(role, img_data):
    img = pygame.image.load(StringIO(img_data))
    DISPLAYSURF.blit(img, (get_x(role), Y_IMAGE))


def draw_compass(role, head):
    x_compass = get_x(role) + IMG_W / 2
    pos_center = (x_compass, Y_COMPASS)
    pos_north = (x_compass, Y_COMPASS - IMG_H / 2.03)
    # circle plate
    pygame.draw.circle(DISPLAYSURF, GRAY, pos_center, IMG_H / 2)
    pygame.draw.circle(DISPLAYSURF, DARK_GRAY, pos_center, int(IMG_H / 2.03))

    head = -head - 90  # top is north.

    # north line
    pygame.draw.line(DISPLAYSURF, RED, pos_center, pos_north, 1)
    # direction
    pos_end = (pos_center[0] + cos(radians(head)) * int(IMG_H / 2.1),
               pos_center[1] + sin(radians(head)) * int(IMG_H / 2.1))
    pygame.draw.line(DISPLAYSURF, LIGHT_GRAY, pos_center, pos_end, 2)


def draw_wall(role, head, urg_data):
    x_compass = get_x(role) + IMG_W / 2
    def transform(x, y):
        return x + x_compass, Y_COMPASS - y
    data = list(urg_data)
    for i, r in enumerate(data):
        r = (IMG_H / 3) * r / 4000.0
        theta = radians((i * 180.0 / 512.0) + head)
        data[i] = p2c(r, theta)

    px_p, py_p = 0, 0
    for i, (x, y) in enumerate(data + [(0, 0)]):
        px, py = x, y
        pygame.draw.line(
            DISPLAYSURF,
            BLUE,
            transform(px_p, py_p),
            transform(px, py),
            3)
        px_p, py_p = px, py

    wall = calc_front_wall_for_draw(data)
    px_p, py_p = 0, 0
    for i, (x, y) in enumerate(wall + [(0, 0)]):
        px, py = x, y
        pygame.draw.line(
            DISPLAYSURF,
            GREEN,
            transform(px_p, py_p),
            transform(px, py),
            3)
        px_p, py_p = px, py


elapsed_time_rect = (X_FONT, MARGIN, 300, 32)
def draw_elapsed_time(diff_time, is_record):
    pygame.draw.rect(DISPLAYSURF, BLACK, elapsed_time_rect)
    diff_time = int(diff_time)
    minute, second = diff_time // 60, diff_time % 60
    rec_mark = u"R" if is_record else u""
    font_rendered = font_caption.render(u"[elapsed time] %02d:%02d %s" % (minute, second, rec_mark), False, LIGHT_GRAY)
    DISPLAYSURF.blit(font_rendered, (X_FONT, MARGIN))

rssi_surface = pygame.Surface((width, 240))
rssi_surface.set_alpha(10)
rssi_surface.fill(BLACK)
def draw_rssi_alpha():
    DISPLAYSURF.blit(rssi_surface, (0, Y_RSSI + 100 - 64))
    for role in ('RED', 'GREEN'):
        draw_rssi_axes(get_x(role), Y_RSSI + 100)
        draw_rssi_axes(get_x(role), Y_RSSI + 100 + 64 + MARGIN, bro=True)


def draw_value(rssi, x, bro, color):
    if bro:
        x += 100
        bro_text = "[bros] "
    else:
        bro_text = ""
    font_rendered = font_caption.render(bro_text + str(rssi) + 'dBm', False, color)
    DISPLAYSURF.blit(font_rendered, (x, Y_RSSI + 180))


def draw_rssi_axes(x, y, bro=False):
    # axis bottom
    pygame.draw.line(DISPLAYSURF, GRAY, (x, y), (x + IMG_W, y), 1) 
    # axis top
    pygame.draw.line(DISPLAYSURF, GRAY, (x, y - 64), (x + IMG_W, y - 64), 1)
    # RSSI_THRESHOLD
    if bro:
        rt = 64 + RSSI_BRO_THRESHOLD / 2
    else:
        rt = 64 + RSSI_THRESHOLD / 2
    pygame.draw.line(
        DISPLAYSURF,
        RED,
        (x, y - rt),
        (x + IMG_W, y - rt), 1)

def draw_rssi_graph(rssi, x, y, bro=False):
    count = int(time.time() * 20 % IMG_W)
    threshold = RSSI_BRO_THRESHOLD if bro else RSSI_THRESHOLD
    if rssi >= threshold:
        value_color = GREEN
    else:
        value_color = LIGHT_GRAY
    draw_value(rssi, x, bro, value_color)
    rssi = 64 + rssi / 2
    # value
    pygame.draw.line(DISPLAYSURF, value_color, (x + count, y), (x + count, y - rssi), 2)

def draw_names():
    for name in ('GREEN', 'RED'):
        font_rendered = font_caption.render(name, False, get_color(name))
        DISPLAYSURF.blit(font_rendered, (get_x(name), MARGIN))

if True:

    def printing_handler(message, address):
        print "[INFO] Got %s from %s" % (message, address)

    def catch_img(message, address):
        role, img_data = message.getValues()[:2]
        # preview jpg image to screen
        draw_camera_image(role, img_data)

    def catch_compass(message, address):
        # get compass
        role, head, temperature = message.getValues()[:3]
        if int(time.time()) % 5 == 0:
            print "[%s] compass: %s" % (role, str(head))
            print "[%s] temperature: %0.1f" % (role, temperature)
        shared_memory['compass_head'] = head
        draw_compass(role, head)
        if role == 'RED':
            draw_wall(role, head, shared_memory['urg_red'])
        elif role == 'GREEN':
            draw_wall(role, head, shared_memory['urg_green'])
        draw_rssi_alpha()

    def catch_urg_data(message, address):
        role, compressed_urg_data = message.getValues()[:2]
        urg_data = decompress_urg_data(compressed_urg_data)
        if role == 'RED':
            shared_memory['urg_red'] = urg_data
        elif role == 'GREEN':
            shared_memory['urg_green'] = urg_data

    def catch_urg_nearby(message, address):
        role, min_distance = message.getValues()
        print "[INFO] %s's min distance is %s" % (role, min_distance)
        if shared_memory['is_playing_nearby'] and not shared_memory['is_playing']:
            shared_memory['is_playing'] = True
            play_remm()
            # register ConversationData record
            cd = ConversationData()
            cd.title = shared_memory['remm_work_pk']
            cd.min_distance = min_distance
            cd.min_role = role
            cd.save()
            print "[INFO] save DB."

    def catch_detected_area(message, address):
        role, area = message.getValues()[:2]
        print "[detect] %s: %s" % (role, str(area))

    def catch_detected_stop(message, address):
        role = message.getValues()[0]
        area = message.getValues()[1:]
        print "[detectstop] %s stopped!!!: %s" % (role, str(area))

    def catch_rssi(message, address):
        role, xbee_addr, rssi = message.getValues()[:3]
        rssi = int(rssi)
        draw_rssi_graph(rssi, get_x(role), Y_RSSI + 100)

    def catch_rssi_bro(message, address):
        role, xbee_addr, rssi = message.getValues()[:3]
        rssi = int(rssi)
        draw_rssi_graph(rssi, get_x(role), Y_RSSI + 100 + 64 + MARGIN, bro=True)

    def catch_temperature(message, address):
        role, temperature = message.getValues()[:2]
        print "[%s] temperature: %s C" % (role, temperature)

    def catch_action_end(message, address):
        print "[catch_action_end]", message.getValues()
        reliable_stuff = message.getValues()[1]
        if shared_memory['reliable_stuff'] == reliable_stuff:
            return  # 更新しない
        old_action_end = shared_memory['action_end']
        while old_action_end == shared_memory['action_end']:
            shared_memory['action_end'] = time.time()
            shared_memory['reliable_stuff'] = reliable_stuff
            time.sleep(0.01)

    def catch_kinect(message, address):
        values = message.getValues()
        print "[catch_kinect]", values

        if values[0] == 'detect':
            # stop a play
            if shared_memory['is_playing'] and \
               shared_memory['remm_work_pk'] != 0:  # not camera
                print "stop playing!!!"
                shared_memory['stop_playing'] = True
                shared_memory['remm_return_pk'] = shared_memory['remm_work_pk']
                shared_memory['remm_work_pk'] = 0

            if not shared_memory['is_playing'] and \
               shared_memory['cameraman_last_play'] < (time.time() - CAMERAMAN_TIMESPAN):
                # freeze!
                time.sleep(2)
                play_content(K_h)   # camera
                print "play_content end!!!!!!!!!!!!!!!!"
                shared_memory['cameraman_last_play'] = time.time()


    Me.add_osc_listener("/print", printing_handler)
    Me.add_osc_listener("/info", printing_handler)
    Me.add_osc_listener("/img", catch_img)
    Me.add_osc_listener("/compass", catch_compass)
    Me.add_osc_listener("/urg_data", catch_urg_data)
    Me.add_osc_listener("/urg_nearby", catch_urg_nearby)
    Me.add_osc_listener("/detect", catch_detected_area)
    Me.add_osc_listener("/detectstop", catch_detected_stop)
    Me.add_osc_listener("/rssi", catch_rssi)
    Me.add_osc_listener("/rssi_bro", catch_rssi_bro)
    Me.add_osc_listener("/temperature", catch_temperature)
    Me.add_osc_listener("/action_end", catch_action_end)
    Me.add_osc_listener("/kinect", catch_kinect)

    Me.listenTCP()
    print "Starting OSCServer. Use ctrl-C to quit."

    # pygame setup
    pygame.init()
    pygame.font.init()
    font_caption = pygame.font.Font(None, 32)
    font_caption.set_bold(1)

    DISPLAYSURF = pygame.display.set_mode((width, height), 0, 32)
    pygame.display.set_caption("osc test")
    DISPLAYSURF.fill(BLACK)

    ########################### main loop ##################################
    is_record = False
    start_time = time.time()
    draw_elapsed_time(0, is_record)
    draw_names()

    draw_compass('RED', 0)
    draw_compass('GREEN', 0)

    # loop
    pygame_call = LoopingCall(pygame_loop, start_time, is_record, shared_memory)
    finishedDeferred = pygame_call.start(1 / 30.0, now=False)
    finishedDeferred.addCallback(lambda ign: pygame_call.stop())
    Me.reactor_run()
    Me.close()
