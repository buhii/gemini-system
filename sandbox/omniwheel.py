"""
omniwheel.py - settings for omniwheel
"""
# -*- coding: utf-8 -*-
from threading import Thread
import time
import serial
running = True
DURATION = 0.10   # sec

STRAIGHT_ALPHA = 0.05
STRAIGHT_MAX_SPEED = 5
ROTARY_ALPHA = 17.3
ROTARY_MAX_SPEED = 3


omni = serial.Serial('/dev/ttyUSB2', parity=serial.PARITY_EVEN, baudrate=38400)


class OmniWheel(Thread):
    def __init__(self, is_rotary=False, is_counter=False, step=1.0):
        Thread.__init__(self)
        self.is_rotary = is_rotary
        self.is_counter = is_counter
        self.step = step

    def run(self):
        if self.is_rotary:
            self.turn(self.step, self.is_counter)
        else:
            self.go_straight(self.step, self.is_counter)

    def go_straight(self, distance, is_backward=False):
        check_distance(distance)
        rest_time = distance / (5 * STRAIGHT_ALPHA) - 0.5
        timeline = make_timeline(rest_time, STRAIGHT_MAX_SPEED)

        for duration, speed in timeline:
            if not running:
                print "not running"
                break

            if is_backward:
                out_command("1000xx00", speed)
            else:
                out_command("0100xx00", speed)
            time.sleep(duration)

    def turn(self, degree, is_counter=False):
        check_degree(degree)
        rest_time = degree / (3 * ROTARY_ALPHA) - 0.3
        timeline = make_timeline(rest_time, ROTARY_MAX_SPEED)

        for duration, speed in timeline:
            if not running:
                break

            if is_counter:
                out_command("1111xxx0", speed)
            else:
                out_command("0000xxx0", speed)
            time.sleep(duration)


def out_command(cmd, param):
    cmd = cmd.replace('x', str(param))
    cmd += '\x0d'
    omni.write(cmd)
    return cmd


def make_timeline(rest_time, division):
    """
    make timeline
    - [(duration1, speed1), (duration2, speed2), ...]
    """
    ret = []
    for i in range(division):
        ret.append((DURATION, i))

    if rest_time > 0:
        ret.append((rest_time, division))

    for i in reversed(range(division)):
        ret.append((DURATION, i))

    return ret


def _cleanup():
    print("Cleaning up")
    stop()


def stop():
    global running
    running = False


def check_distance(distance):
    if int(distance) > 3.0:
        raise ValueError("%d is too long. max distance is 3.0m" % distance)


def check_degree(degree):
    if int(degree) > 360:
        raise ValueError("%d is too big. max degree is 360." % degree)


def B(distance):
    OmniWheel(is_rotary=False, is_counter=False, step=distance).start()


def F(distance):
    OmniWheel(is_rotary=False, is_counter=True, step=distance).start()


def L(degree):
    OmniWheel(is_rotary=True, is_counter=True, step=degree).start()


def R(degree):
    OmniWheel(is_rotary=True, is_counter=False, step=degree).start()

def omniwheel_stop():
    out_command("00000000", 0)
