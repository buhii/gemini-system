import time
import sys
import curses
from omniwheel import out_command, omniwheel_stop



def endwin(result=None, f=None):
    if result and f:
        # write to file
        pass

    # deinitialize curses
    curses.nocbreak()
    curses.echo()
    curses.endwin()
    sys.exit()


if __name__ == '__main__':
    # initialize curses
    stdscr = curses.initscr()
    stdscr.nodelay(1)
    curses.cbreak()
    curses.raw()
    result = None

    while True:
        stdscr.addstr(0, 0, "f, b, z, l, r, q")
        stdscr.refresh()
        time.sleep(0.1)
        speed = 3

        stdscr.refresh()
        c = stdscr.getch()

        if c == ord('f') or c == curses.KEY_UP:
            out_command("1000xx00", speed)  # F
        elif c == ord('b'):
            out_command("0100xx00", speed)  # B
        elif c == ord('l'):
            out_command("1111xxx0", 3)  # L
        elif c == ord('r'):
            out_command("0000xxx0", 3)  # R
        elif c == ord('q'):
            out_command("00000000",0)
            stdscr.addstr(1, 0, "quit!", curses.A_REVERSE)
            endwin(result)
        elif 48 < c and c < 127:
            out_command("00000000",0)

    stdscr.erase()

    # closing
    endwin(result)
