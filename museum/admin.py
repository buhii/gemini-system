# -*- coding: utf-8 -*-
from django.contrib import admin
from museum.models import Exhibition, XBeeBeacon, Work, Figure, Action, ServoMotion


class ExhibitionAdmin(admin.ModelAdmin):
    fields = ('name', 'date_start', 'date_end')
    list_display = ('name', 'date_start', 'date_end')
admin.site.register(Exhibition, ExhibitionAdmin)


class XBeeBeaconAdmin(admin.ModelAdmin):
    fields = ('name', 'serial_id', 'source_addr', 'rssi_threshold')
    list_display = ('name', 'serial_id', 'source_addr', 'rssi_threshold')
admin.site.register(XBeeBeacon, XBeeBeaconAdmin)


class FigureInline(admin.TabularInline):
    model = Figure
    readonly_fields = ('admin_thumbnail_img', 'admin_status', 'display_doisize')
    fields = ('image', 'admin_thumbnail_img', 'admin_status', 'display_doisize', 'high',)


class ActionInline(admin.TabularInline):
    model = Action
    fields = ('priority',
              'performer',
              'timespan',
              'sound',
              'servo',
              'omni_direction',
              'omni_timespan',
              'memo'
              )

class WorkAdmin(admin.ModelAdmin):
    fields = ('priority', 'exhibition', 'name', 'beacon', 'scenario', 'is_valid', 'memo')
    list_display = ('name', 'exhibition', 'priority', 'beacon', 'is_valid')
    inlines = [FigureInline, ActionInline]
admin.site.register(Work, WorkAdmin)


class ServoMotionAdmin(admin.ModelAdmin):
    fields = ('performer', 'name', 'code')
    list_display = ('performer', 'name', 'code')
admin.site.register(ServoMotion, ServoMotionAdmin)
