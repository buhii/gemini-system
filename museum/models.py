# -*- coding: utf-8 -*-
import os.path

from PIL import Image

from django.db import models
from django.core.files.storage import FileSystemStorage
from django.conf import settings

from utils.fields import BlobField, ContentTypeRestrictedFileField


class Exhibition(models.Model):
    """
    Exhibition model
    """
    class Meta:
        verbose_name = u"展示会"
        verbose_name_plural = u"展示会"

    def __unicode__(self):
        return u"%s (%s 〜 %s)" % (self.name, self.date_start, self.date_end)

    name = models.CharField(max_length=127, verbose_name=u"展示名")
    date_start = models.DateField(verbose_name=u"開催日")
    date_end = models.DateField(verbose_name=u"終了日")

    def get_scenario_path(self):
        return os.path.join(settings.LOCAL_OPERATION_DIR, "exhibition_%d.yml" % self.pk)


class XBeeBeacon(models.Model):
    """
    XBee beacon model
    """
    class Meta:
        verbose_name = u"XBee"
        verbose_name_plural = u"XBee"

    name = models.CharField(max_length=127, verbose_name=u"モジュール名")
    serial_id = models.CharField(max_length=127, verbose_name=u"ID")
    source_addr = models.CharField(max_length=4, verbose_name=u"source address", default="", help_text=u"16bit address (MY)")
    rssi_threshold = models.IntegerField(verbose_name=u"RSSI のしきい値", default=-49, help_text=u"この値以上の電波強度であれば 約1m 以内に作品が存在すると仮定します")

    def __unicode__(self):
        return self.name


class Work(models.Model):
    """
    Work model
    """
    class Meta:
        verbose_name = u"作品"
        verbose_name_plural = u"作品"
        ordering = ['exhibition', 'priority']

    def __unicode__(self):
        return self.name

    exhibition = models.ForeignKey(Exhibition, verbose_name=u"展示会")
    name = models.CharField(max_length=127, verbose_name=u"作品名")
    beacon = models.ForeignKey(XBeeBeacon, verbose_name=u"設置する XBee モジュール")
    scenario = models.URLField(verbose_name=u"ロボットの会話シナリオ",
                               help_text=u"Google Spreadsheet の URL を入力してください",
                               default='https://docs.google.com/a/buhii.org/spreadsheet/ccc?key=0As7IzPe9tebQdFJORy1ib2lYUTFGdU5KUEdodWtWcEE#gid=0')
    memo = models.TextField(verbose_name=u"メモ", blank=True, null=True)
    priority = models.IntegerField(verbose_name=u"作品の順番")
    is_valid = models.BooleanField(verbose_name=u"有効にしますか？", default=True)


image_fs = FileSystemStorage(
    location=os.path.join(settings.STATICFILES_DIRS[0]),
    base_url=settings.STATIC_URL,
)
def get_local_doi_path(pk, work_pk):
    return os.path.join(settings.LOCAL_OPERATION_DIR, "%s-%s.doi" % (work_pk, pk))


class Figure(models.Model):
    """
    Work's image
    """
    STATUS_NONE = "X"
    STATUS_PROCESS = '-'
    STATUS_PENDING = 'P'
    STATUS_ERROR = 'R'
    STATUS_COMPLETE = 'C'
    DOI_PROCESS_STATUS = (
        (STATUS_NONE, u'(なし)'),
        (STATUS_PROCESS, u'生成中'),
        (STATUS_PENDING, u'ペンディング'),
        (STATUS_COMPLETE, u'生成完了'),
        (STATUS_ERROR, u'エラー'),
        )
    class Meta:
        verbose_name = u"作品画像"
        verbose_name_plural = u"作品画像"

    work = models.ForeignKey(Work)
    image = models.ImageField(verbose_name=u"画像", storage=image_fs, upload_to='img')
    thumbnail = BlobField(verbose_name=u"work's thumbnail picture")
    doi = BlobField(verbose_name=u"Fern-based planar object Detector Data")
    status = models.CharField(max_length=1, default=STATUS_NONE, choices=DOI_PROCESS_STATUS, verbose_name=u"検出器")
    doisize = models.PositiveIntegerField(verbose_name=u"検出器のサイズ", default=0)
    high = models.PositiveIntegerField(verbose_name=u"作品画像の高さ(cm)", default=0)
    checksum = models.CharField(max_length=512, default="", blank=True, null=True, verbose_name=u"チェックサム")

    def __unicode__(self):
        return self.work.name

    def thumbnail_url(self):
        seq = self.image.url.split('/')
        seq[-1] = 't_' + seq[-1]
        return '/'.join(seq)

    def local_doi_path(self):
        return get_local_doi_path(self.pk, self.work.pk)

    def display_doisize(self):
        return "%.2f MB" % (self.doisize / 1024.0 / 1024.0)
    display_doisize.short_description = u"検出器のサイズ"

    def admin_status(self):
        color_dict = {
            self.STATUS_NONE:    '#000000',
            self.STATUS_PROCESS: '#2233aa',
            self.STATUS_PENDING: '#77ccdd',
            self.STATUS_COMPLETE:'#33aa44',
            self.STATUS_ERROR:   '#aa4433',
            }
        return "<span style='color: %s'>%s</a>" % (
            color_dict[self.status],
            self.get_status_display())
    admin_status.short_description = u"検出器"
    admin_status.allow_tags = True

    def thumbnail_path(self):
        directory, filename = os.path.split(self.image.path)
        return os.path.join(directory, 't_' + filename)

    def save(self, *args, **kwargs):
        first_save = not bool(self.pk)
        super(Figure, self).save(*args, **kwargs)
        if first_save:
            # make thumbnail image
            img = Image.open(self.image.path)
            size = (settings.THUMBNAIL_IMAGE_SIZE, settings.THUMBNAIL_IMAGE_SIZE)
            img.thumbnail(size)
            img.save(self.thumbnail_path())

            # add task to create doi
            from museum.tasks import FernBasedDetectorGenerator as FBDG
            FBDG.delay(figure=self)

    def admin_thumbnail_img(self):
        return "<a href=\"%s\"><img src=\"%s\"/></a>" % (
            self.image.url, self.thumbnail_url())
    admin_thumbnail_img.short_description = u"サムネイル"
    admin_thumbnail_img.allow_tags = True


def get_local_sound_path(work_pk, filename):
    filename = os.path.split(filename)[-1]   # 余分なディレクトリ文字列を削除する
    return os.path.join(settings.LOCAL_OPERATION_DIR, "%s/%s" % (work_pk, filename))


def sound_path(instance, filename):
    return 'sound/%s/%s' % (instance.work.pk, filename)


class SoundFile(models.Model):
    """
    作品に属する音声ファイル
    """
    class Meta:
        verbose_name = u"音声ファイル"
        verbose_name_plural = u"音声ファイル"
        unique_together = ('work', 'sound')

    work = models.ForeignKey(Work)
    sound = ContentTypeRestrictedFileField(
        verbose_name=u"音声ファイル(mp3, ogg)",
        storage=image_fs,
        upload_to=sound_path,
        content_types=('audio/ogg', 'video/ogg', 'application/ogg', 'audio/mpeg', 'audio/mp3','audio/vnd.wave'),
        max_upload_size=20971520)
    memo = models.CharField(max_length=255, verbose_name=u"メモ", blank=True)

    def __unicode__(self):
        return self.sound.name

    def local_path(self):
        return get_local_sound_path(self.work.pk, self.sound.name)


GEMINI_BLACK = "B"
GEMINI_GRAY = "A"
GEMINI_CHOICES = (
    (GEMINI_BLACK, u"B(ブラック/Red)"),
    (GEMINI_GRAY, u"A(グレイ/Green)"),
    )


class ServoMotion(models.Model):
    class Meta:
        verbose_name = u"モーション"
        verbose_name_plural = u"モーション"
        ordering = ['performer']

    def __unicode__(self):
        return u"%s - %s" % (self.performer, self.name)

    performer = models.CharField(max_length=1, default=GEMINI_GRAY, choices=GEMINI_CHOICES, verbose_name=u"語り手")
    name = models.CharField(max_length=255, verbose_name=u"モーション名")
    code = models.CharField(max_length=1023, verbose_name=u"ProgramCode")


class Action(models.Model):
    class Meta:
        verbose_name = u"モーション"
        verbose_name_plural = u"モーション"
        ordering = ['priority']

    def __unicode__(self):
        return ''

    OMNI_STOP = 'X'
    OMNI_FORWARD = 'F'
    OMNI_LEFT = 'L'
    OMNI_RIGHT = 'R'
    OMNI_BACKWARD = 'B'
    OMNI_COMPASS = 'C'
    OMNI_CHOICES = (
        (OMNI_STOP, u'-'),
        (OMNI_FORWARD, u'前'),
        (OMNI_BACKWARD, u'後'),
        (OMNI_LEFT, u'左回転'),
        (OMNI_RIGHT, u'右回転'),
        (OMNI_COMPASS, u'方位'),
        )
    work = models.ForeignKey(Work)
    priority = models.FloatField(verbose_name=u"優先順位", default=0.0)
    performer = models.CharField(max_length=1, default=GEMINI_GRAY, choices=GEMINI_CHOICES, verbose_name=u"語り手")
    timespan = models.FloatField(verbose_name=u"時間間隔(秒)", default=0.0)
    sound = ContentTypeRestrictedFileField(
        verbose_name=u"音声ファイル(mp3, ogg)",
        storage=image_fs,
        upload_to=sound_path,
        content_types=('audio/ogg', 'video/ogg', 'application/ogg', 'audio/mpeg', 'audio/mp3','audio/vnd.wave'),
        max_upload_size=20971520,
        blank=True,
        null=True)
    servo = models.ForeignKey(ServoMotion, blank=True, null=True, verbose_name=u"モーション")
    omni_direction = models.CharField(max_length=1, default=OMNI_STOP, choices=OMNI_CHOICES, verbose_name=u"オムニホイール(方向)")
    omni_timespan = models.FloatField(verbose_name=u"オムニホイール(秒)", default=0.0)
    memo = models.CharField(max_length=255, verbose_name=u"メモ", blank=True)

    def local_sound_path(self):
        return get_local_sound_path(self.work.pk, self.sound.name)

    def save(self, *args, **kwargs):
        if not self.pk and self.priority == 0.0:
            # auto-increment priority
            max_priority = Action.objects.order_by('-priority')[0].priority
            self.priority = max_priority + 1.0
        super(Action, self).save(*args, **kwargs)
