# -*- coding: utf-8 -*-
from django.forms import Form, ValidationError, FileField, ChoiceField
from museum.models import GEMINI_CHOICES

def is_relevant_xml(f):
    """
    適切な xml ファイルかどうか調べる
    """
    if not f.name.endswith(".xml"):
        raise ValidationError(u"XML 形式以外は登録できません - \"%s\"" % f.name)

    data=f.read()
    f.seek(0)
    if len(data) > 1024 * 500:
        raise ValidationError(u"ファイルが大きすぎます。添付できるのは500KBまでです")
    return data


class XMLForm(Form):
    file = FileField(label=u"Heart To Heart のモーション XML ファイル")
    performer = ChoiceField(choices=GEMINI_CHOICES, label=u"語り手")
    def __init__(self, *args, **kwargs):
        super(XMLForm, self).__init__(*args, **kwargs)

    def clean_file(self):
        f = self.cleaned_data['file']
        is_relevant_xml(f)
