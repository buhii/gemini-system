# -*- coding: utf-8 -*-
import gc
import hashlib
import os
from multiprocessing import Process
from shutil import rmtree
from tempfile import mkdtemp

from PIL import Image

from django.conf import settings
from museum.models import Figure
from celery.task import Task

from pyferns import planar_pattern_detector_wrapper as FernsDetector

max_image_size = tuple([settings.FIGURE_IMAGE_MAX_SIZE] * 2)
PROCESS_DEBUG = True


def generate_detector(img_path, doi_path):
    # img_path の画像から指定されたパスに検出器を生成する
    fd = FernsDetector()
    if PROCESS_DEBUG: print "[INFO] FernsDetector created."
    fd.learn(str(img_path))
    if PROCESS_DEBUG: print "[INFO] learning done."
    fd.save(str(doi_path))
    if PROCESS_DEBUG: print "[INFO] generate_doi end."


class FernBasedDetectorGenerator(Task):
    """
    画像から Fern-based 特徴検出器を構築する
    """
    def run(self, figure):
        self.figure = figure
        self.init_sources()
        self.change_status(Figure.STATUS_PENDING)
        os.chdir(self.tempdir)

        try:
            self.generate_doi()
            self.save_db()
        except Exception as e:
            print "[ERROR] %s" % e
            self.change_status(Figure.STATUS_ERROR)
        finally:
            self.finalize()

    def change_status(self, status):
        self.figure.status = status
        self.figure.save()

    def generate_doi(self):
        # 画像ファイルから検出器を構築する
        # 現状メモリリークが起きているので、multiprocessing で管理
        self.change_status(Figure.STATUS_PROCESS)
        p = Process(target=generate_detector,
                    args=(self.tmpimg_path, self.tmpdoi_path))
        p.start()
        p.join()
        if PROCESS_DEBUG: print "[INFO] generate_detector joined."

    def save_db(self):
        # 検出器を DB に格納する
        doifile = open(self.tmpdoi_path)
        doidata = doifile.read()
        doifile.close()
        self.figure.doi = doidata
        self.figure.doisize = len(doidata)
        self.figure.checksum = hashlib.sha224(doidata).hexdigest()
        self.change_status(Figure.STATUS_COMPLETE)
        del doidata

    def get_abspath(self, filename):
        return os.path.join(self.tempdir, filename)

    def init_sources(self):
        "一時ディレクトリを作成し, 画像を縮小し保存する"
        self.tempdir = mkdtemp()
        img_path = self.figure.image.path
        img_filename = os.path.split(img_path)[1]
        self.tmpimg_path = self.get_abspath(img_filename)
        self.tmpdoi_path = os.path.splitext(self.tmpimg_path)[0] + '.doi'
        img = Image.open(img_path)
        img.thumbnail(max_image_size)
        img.save(self.tmpimg_path)

    def finalize(self):
        # 一時ディレクトリ内のファイルを全て削除
        if PROCESS_DEBUG: print os.listdir(self.tempdir)
        rmtree(self.tempdir)
        # モデル参照も GC されるよう削除しておく
        del self.figure
        print "[INFO] gc.collect: %s" % repr(gc.collect())
        if PROCESS_DEBUG: print "[INFO] Task End!"
