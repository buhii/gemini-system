# -*- coding: utf-8 -*-
import gc
import os.path
from math import radians
import time
from datetime import datetime
from multiprocessing import Process, Manager
from utils.afsm import AFSM
from utils.devices import Me
from utils.lsm303 import LSM303DLH
from utils.commons import infinite_loop, RingBuffer, assign_shared_memory, convert_servo_command, compress_urg_data, compass_diff, calc_front_wall_for_servo, polar2cartesian as p2c
from utils.beacon import get_rssi
from utils.camera import gemini_camera
from utils.omniwheel import omni_basic, get_min_length, MAX_LENGTH
from utils.kondo import RCB_4HV, FaceMotion
from gemini_system.universal import SENTINELS_COMPASS, WORKS_COMPASS, RSSI_THRESHOLD, RSSI_MIN, LOCAL_OPERATION_DIR
from pyliburg import urg_wrapper as UrgDevice
from record.models import Compass, URGData, URGDetailData, FaceMotionData, AFSMData
from txosc import osc


FACE_YAW_WALL_SERVO_SPEED = 127
DEBUG_NO_DETECTOR = False
RSSI_ADDR_BUF_LENGTH = 24
COMPASS_BUF_LENGTH = 16
TARGET_FOUND_GUARD_TIME = 11.3
NEARBY_DISTANCE = 1500


def _print(v):
    print repr(v)


def force_change_afsm_status(shared_memory, status):
    # 強制的に AFSM_STATUS を変更する
    while shared_memory['AFSM_STATUS'] != status:
        shared_memory['AFSM_STATUS'] = status
        time.sleep(0.01)


def is_emergency_urg(urg_data):
    return not bool(filter(lambda value: value != -1, urg_data))

urg = UrgDevice('/dev/ttyACM0')   # operator and gemini are same path
@infinite_loop
def half_range_urg(me, shared_memory):
    # set raw urg data
    t = urg.capture()
    t = list(reversed(t))
    shared_memory['urg_data'] = t

    # emergency stop
    if is_emergency_urg(t):
        print "*" * 60
        print "  EMERGENCY STOP  ".center(60, '*')
        print "  URG connection maybe lost!  ".center(60, '*')
        print "*" * 60
        for i in xrange(100000):
            shared_memory['AFSM_STATUS'] = AFSM.STOP
            time.sleep(0.1)
            # if ok, we return!
            t = urg.capture()
            t = list(reversed(t))
            shared_memory['urg_data'] = t
            if not is_emergency_urg(t):
                print "[INFO] URG status returns normal!"
                break

    # set 8div urg data
    urg_data_8div_old = shared_memory['urg_data_8div']
    v = []
    for i in range(8):
        d = get_min_length(t[i * 64: i * 64 + 63])
        v.append(d)
    shared_memory['urg_data_8div'] = v
    if shared_memory['is_record'] == True:
        urgdata = URGData()
        urgdata.array = v
        urgdata.save()

    if (time.time() - int(time.time())) % 0.5 < 0.1:  # every 0.5 sec
        # send urg data
        msg = osc.Message('/urg_data')
        compressed_urg_data = compress_urg_data(t)
        udd = URGDetailData(urg_array=compressed_urg_data)
        udd.save()
        msg.add(osc.BlobArgument(compressed_urg_data))
        me.send(me.operator, msg)

        # send nearby information
        min_urg_front = min(v[3:5])
        if min_urg_front < NEARBY_DISTANCE:
            msg = osc.Message('/urg_nearby')
            msg.add(min_urg_front)
            me.send(me.operator, msg)

    """
    see wall
    """
    IMG_H = 192

    cartesian_data = list(t)
    for i, r in enumerate(cartesian_data):
        r = (IMG_H / 3) * r / 4000.0
        theta = radians(i * 180.0 / 512.0)
        cartesian_data[i] = p2c(r, theta)

    old_face_yaw_wall_dir = shared_memory['face_yaw_wall_dir']
    shared_memory['face_yaw_wall_dir'] = calc_front_wall_for_servo(cartesian_data)

    # record facemotion data
    if old_face_yaw_wall_dir != shared_memory['face_yaw_wall_dir']:
        fmd = FaceMotionData()
        fmd.pitch = shared_memory['face_pitch']
        fmd.yaw = shared_memory['face_yaw_wall_dir']
        fmd.save()


lsm303 = LSM303DLH(Me.compass)
@infinite_loop
def read_compass(me, shared_memory):
    lsm303.read()
    if (time.time() - int(time.time())) % 1.0 < 0.05:  # every 1.0 sec
        # send osc.Message
        msg = osc.Message('/compass')
        msg.add(lsm303.head)
        msg.add(lsm303.temperature)
        me.send(me.operator, msg)

        # save shared memory
        shared_memory['compass'] = lsm303.head
        # save compas_buf only running straight.
        if (shared_memory['AFSM_STATUS'] not in (AFSM.TURN_TO, AFSM.FOUND_XBEE)) or \
            not (shared_memory['random_walk'] < time.time() < shared_memory['random_walk'] + 4):
            def update_compass_buf(d): d.append(lsm303.head)
            assign_shared_memory(shared_memory, 'compass_buf', update_compass_buf)

        # save DB
        if shared_memory['is_record'] == True:
            c = Compass(head=int(lsm303.head))
            c.save()
        time.sleep(0.01)


def process_AFSM(me, shared_memory, shared_rssi):
    detected_signals = []
    old_area = []

    while True:
        time.sleep(0.05)
        AFSM_STATUS = shared_memory['AFSM_STATUS']
        if shared_memory['is_record'] == True:
            afsmdata = AFSMData()
            afsmdata.status = AFSM_STATUS
            afsmdata.save()

        if AFSM_STATUS in (AFSM.STOP, AFSM.LOADING, AFSM.DETECT):
            detected_signals = []
            old_area = []
            continue

        """
        catch detected_area
        """
        tmp_area = []
        for i in range(8):
            tmp_area.append(shared_memory['detected_area'][i])
        if bool(filter(lambda v: v != 0, tmp_area)) and tmp_area != old_area:
            detected_signals.append((time.time(), True))
            old_area = tmp_area[:]
            print old_area
        else:
            detected_signals.append((time.time(), False))

        valid_signals = len(filter(lambda tpl: tpl[1], detected_signals))
        #print 'valid_signals', valid_signals
        # total_signals = len(detected_signals)
        if valid_signals >= 5:
            print " detected ".center(60, '-')
            msg = osc.Message('/detectstop')
            for v in tmp_area:
                msg.add(v)
            me.send(me.operator, msg)
            me.send(me.brother, msg)  # send to brother!
            me.force_stop()
            # must stop!
            force_change_afsm_status(shared_memory, AFSM.DETECT)
            time.sleep(1)
            me.force_stop()
            force_change_afsm_status(shared_memory, AFSM.DETECT)
            # STOP!
            msg = osc.Message('/stop')
            me.send(me.ip, msg)
            continue
            """
            # 絵の重心から移動するステートに入る!
            """
            pass

        # 古いシグナルを消す(12秒前で...)
        detected_signals = filter(lambda tpl: tpl[0] > (time.time() - 12), detected_signals)

        """
        RSSI を調べる
        """
        if AFSM_STATUS != AFSM.WANDER:
            continue
        print shared_rssi['data']
        #print "[NOW]", datetime.datetime.fromtimestamp(time.time())
        #print "[GUARD]", datetime.datetime.fromtimestamp(guard_time)
        guard_time = shared_rssi['target_guard_time']
        strongest_xbee_addr = shared_rssi['max_addr_list'].most_appeared()
        strongest_xbee_rssi = shared_rssi['data'].get(strongest_xbee_addr)
        if strongest_xbee_rssi > RSSI_THRESHOLD:   # しきい値を超えないと動かないように
            if shared_rssi['target_addr'] == strongest_xbee_addr:
                # 目標ビーコンが最強ビーコンとなった
                #print strongest_xbee_addr, shared_rssi['target_addr']
                #print time.time(), shared_rssi['latest_target_found'] + 5
                if time.time() < (shared_rssi['latest_target_found'] + TARGET_FOUND_GUARD_TIME):
                    # ガードタイム内なので、回転しない
                    #print "[SORRY] RSSI OK. but in guard time!!"
                    pass
                else:
                    # ガードタイムは過ぎている
                    # このときは、身体を１回転させて、近くに画像が存在しないか調べる
                    force_change_afsm_status(shared_memory, AFSM.FOUND_XBEE)
                    shared_rssi['latest_target_found'] = time.time()

            elif strongest_xbee_addr in SENTINELS_COMPASS.keys():
                """
                avoid SENTINEL
                """
                print "[SENTINEL] strongest xbee(%s, %s dBm) is sentinel!(%s)" % (strongest_xbee_addr, strongest_xbee_rssi, datetime.now())

                # もし compass_direction からかなり離れた方角なら、方向転換する
                compass_direction = SENTINELS_COMPASS[strongest_xbee_addr]
                compass_now = shared_memory['compass_buf'].average()
                if compass_diff(compass_now, compass_direction) > 90:
                    shared_memory['compass_dir'] = compass_direction
                    force_change_afsm_status(shared_memory, AFSM.TURN_TO)

                    print "[SENTINEL:%s] very far from compass direction(now: %s, dir: %s): turn!" % (compass_now, compass_direction, strongest_xbee_addr)

                    while shared_memory['AFSM_STATUS'] == AFSM.TURN_TO:
                        # 回転が終わるまで待つ
                        time.sleep(0.1)

                    # 連続して回転しないように、RSSI の値を変更する
                    def set_rssi_min(d): d[strongest_xbee_addr] = RSSI_MIN
                    assign_shared_memory(shared_rssi, 'data', set_rssi_min)

                    # ガードタイムに13.7秒
                    print "[SENTINEL] sentinel(%s) turning end! set 13.7 sec normal guard time." % strongest_xbee_addr
                    shared_rssi['target_guard_time'] = time.time() + 13.7

                else:
                    print "[SENTINEL:%s] not so far!" % strongest_xbee_addr


            elif strongest_xbee_addr in WORKS_COMPASS.keys():
                """
                作品画像へ顔を向ける
                """
                pass

        if time.time() < guard_time:
            # ガードタイム内なら、ずっと wandering だから、そのまま
            continue

        elif AFSM_STATUS == AFSM.VIBRATION_AVOID:
            # 回転しているので、あまり回転しない
            shared_rssi['target_guard_time'] = int(time.time() + 6.0 + 5.0)
            print "vibration avoid, added."
            time.sleep(0.01)


def voice_controller(me, shared_memory):
    """
    音声を再生する
    """
    def get_local_sound_file(path):
        path = path[6:]  # remove 'sound/'
        return os.path.normpath(os.path.join(LOCAL_OPERATION_DIR, path))

    import pygame
    # see: http://ubuntuforums.org/showthread.php?t=1600688
    pygame.mixer.init()
    pygame.mixer.init()

    while True:
        if shared_memory['voice']:
            _time, sound_file_path, reliable_stuff = shared_memory['voice']
            print sound_file_path
            pygame.mixer.music.load(get_local_sound_file(sound_file_path))
            pygame.mixer.music.play()
            time.sleep(float(_time))
            while pygame.mixer.music.get_busy():
                time.sleep(0.1)
            shared_memory['voice'] = None

            # 三回送る
            for i in range(1):
                me.send_action_end(reliable_stuff)
                time.sleep(0.2)
        time.sleep(0.01)


rcb4hv = RCB_4HV(Me)
@infinite_loop
def servo_controller(me, shared_memory):
    """
    サーボを動作させる
    """
    if shared_memory['servo']:
        _time, cmd = shared_memory['servo']
        rcb4hv.write(convert_servo_command(cmd))
        shared_memory['servo'] = None
    elif shared_memory['facemotion']:
        face_motion_direction = shared_memory['facemotion']
        # UP and DOWN
        if face_motion_direction == FaceMotion.UP:
            shared_memory['face_pitch'] += 100
        elif face_motion_direction == FaceMotion.DOWN:
            shared_memory['face_pitch'] -= 100
        if shared_memory['face_pitch'] < 7200:
            shared_memory['face_pitch'] = 7200
        elif shared_memory['face_pitch'] > 8500:
            shared_memory['face_pitch'] = 8500
        print "[INFO] face pitch: ", shared_memory['face_pitch']

        # LEFT and RIGHT
        if face_motion_direction == FaceMotion.LEFT:
            shared_memory['face_yaw'] += 100
        elif face_motion_direction == FaceMotion.RIGHT:
            shared_memory['face_yaw'] -= 100

        if shared_memory['face_yaw'] < 6400:
            shared_memory['face_yaw'] = 6400
        elif shared_memory['face_yaw'] > 8600:
            shared_memory['face_yaw'] = 8600
        print "[INFO] face yaw: ", shared_memory['face_yaw']

        rcb4hv.multi_servo(
            10,
            {1: shared_memory['face_pitch'],
             0: shared_memory['face_yaw']})
        shared_memory['facemotion'] = None

    elif shared_memory['face_yaw_wall_dir'] and \
            shared_memory['face_yaw'] == 7500 and \
            shared_memory['AFSM_STATUS'] == AFSM.WANDER:
        rcb4hv.multi_servo(
            FACE_YAW_WALL_SERVO_SPEED,
            {0: shared_memory['face_yaw_wall_dir']})

    time.sleep(0.05)


@infinite_loop
def omni_controller(me, shared_memory):
    if shared_memory['omni']:
        _time, direction = shared_memory['omni']
        _time, direction = float(_time), int(direction)
        print "@omni_controller: %s, %s" % (_time, direction)
        shared_memory['AFSM_STATUS'] = direction
        time.sleep(_time)
        if shared_memory['AFSM_STATUS'] == direction:
            shared_memory['AFSM_STATUS'] = AFSM.STOP
        shared_memory['omni'] = None
    time.sleep(0.01)


class GeminiBehavior(object):
    def __init__(self):
        self.add_osc_listeners()
        self.manager = Manager()
        self.init_shared_memory()
        self.init_processes()
        self.main_loop()
        self.join_all()

    def init_shared_memory(self):
        self.shared_memory = self.manager.dict(
            urg_data=[0] * 512,
            urg_data_8div=[MAX_LENGTH] * 8,
            straight_count=0,
            detected_area=[0] * 8,
            tracking_work_pk=0,
            AFSM_STATUS=AFSM.STOP,
            brother_detected=False,
            compass=0.0,
            compass_buf=RingBuffer(buf_size=COMPASS_BUF_LENGTH, default=None),
            is_record=False,
            latest_ack=time.time(),
            random_walk=time.time(),
            servo=None,
            omni=None,
            voice=None,
            reliable_stuff=None,
            face_pitch=7500,
            face_yaw=7500,
            facemotion=None,
            face_yaw_wall_dir=7500,
            )
        self.shared_rssi = self.manager.dict(
            latest_get_time=time.time(),
            latest_target_found=time.time(),
            target_guard_time=time.time(),
            brother_addr= None,
            target_addr=None,
            max_addr_list=RingBuffer(buf_size=RSSI_ADDR_BUF_LENGTH, default=None),
            data={},
            )

    def handle_loop(self):
        try:
            Me.reactor_run()
        except KeyboardInterrupt:
            # stop server and omniwheel(critical function)
            Me.close()
            raise KeyboardInterrupt

    def brother_detected(self, message, address):
        print "My brother detected!!!"
        self.shared_memory['brother_detected'] = True

    def emergency_stop(self, message, address):
        print "emergency stop!!"
        Me.force_stop()
        self.shared_memory['AFSM_STATUS'] = AFSM.STOP

    def move(self, message, address):
        direction = message.getValues()[1]
        print "moving: %d" % direction
        if direction == 0:   # X -> STOP
            Me.force_stop()
        self.shared_memory['AFSM_STATUS'] = direction

    def facemotion(self, message, address):
        direction = message.getValues()[1]
        self.shared_memory['facemotion'] = direction

    def wander(self, message, address):
        self.shared_memory['AFSM_STATUS'] = AFSM.WANDER

    def restart(self, message, address):
        print "restart!!"
        self.shared_memory['AFSM_STATUS'] = AFSM.WANDER

    def record_start(self, message, address):
        print "[INFO] db record start!"
        self.shared_memory['is_record'] = True

    def record_stop(self, message, address):
        print "[INFO] db record stop!"
        self.shared_memory['is_record'] = False

    def get_temperature(self, message, address):
        msg = osc.Message('/temperature')
        msg.add(-1)
        Me.send(Me.operator, msg)

    def rotate_direction(self, message, address):
        self.shared_memory['compass_dir'] = float(message.getValues()[1])
        self.shared_memory['AFSM_STATUS'] = AFSM.TURN_TO

    """
    Methods for playing content
    """
    def add_servo(self, message, address):
        _time, cmd = message.getValues()[1:]
        self.shared_memory['servo'] = (_time, cmd)

    def add_voice(self, message, address):
        _time, sound_file_path, reliable_stuff = message.getValues()[1:]
        if self.shared_memory['reliable_stuff'] == reliable_stuff:
            return
        self.shared_memory['voice'] = (_time, sound_file_path, reliable_stuff)
        self.shared_memory['reliable_stuff'] = reliable_stuff

    def add_omni(self, message, address):
        _time, direction = message.getValues()[1:]
        print _time, direction
        self.shared_memory['omni'] = (_time, direction)

    """
    Methods for tracking
    """
    def track_xbee(self, message, address):
        target_xbee_address = message.getValues()[1]
        print "[INFO] target xbee address: %s!" % target_xbee_address
        self.shared_rssi['target_addr'] = target_xbee_address
        print self.shared_rssi['target_addr']

    def track_work(self, message, address):
        """作品探索命令
        """
        print "track_work!!!!!!!!!"
        # 共有メモリ内変数を書き換える
        self.shared_memory['tracking_work_pk'] = int(message.getValues()[1])
        for i in range(8):
            self.shared_memory['detected_area'][i] = 0
        Me.force_stop()
        time.sleep(0.1)
        self.shared_memory['AFSM_STATUS'] = AFSM.LOADING

        # 画像を読み込む. プロセスの切り替えを行う
        self.processes['camera'].terminate()
        time.sleep(0.1)
        print self.processes['camera']
        del self.processes['camera']
        gc.collect()
        print "maybe terminated..... please check camera and memory usage!"

        # 共有メモリを初期化
        self.shared_memory['brother_detected'] = False
        self.shared_memory['face_yaw_wall_dir'] = 7500

        # RSSI の値を初期化
        for attr, value in (('latest_get_time', time.time()),
                            ('target_guard_time', time.time()),
                            ('latest_target_found', time.time()),
                            ('max_addr_list', RingBuffer(buf_size=RSSI_ADDR_BUF_LENGTH, default=None)),
                            ('data', {})):
            self.shared_rssi[attr] = value

        work_pk = int(message.getValues()[1])
        if work_pk == 0 or DEBUG_NO_DETECTOR:  # DEBUG
            work_pk = None

        self.processes['camera'] = Process(
            target=gemini_camera,
            args=(1,   # exhibition pk
                  work_pk,
                  self.shared_memory,
                  ))
        self.processes['camera'].start()

    def catch_ack(self, message, address):
        # ACKを受信した最新日時を記録する
        self.shared_memory['latest_ack'] = time.time()

    def add_osc_listeners(self):
        # specify OSC events
        Me.add_osc_listener('/ack', self.catch_ack)
        Me.add_osc_listener('/track_xbee', self.track_xbee)
        Me.add_osc_listener('/track_work', self.track_work)
        Me.add_osc_listener('/record_start', self.record_start)
        Me.add_osc_listener('/record_stop', self.record_stop)
        Me.add_osc_listener('/rotate_direction', self.rotate_direction)
        Me.add_osc_listener('/stop', self.emergency_stop)
        Me.add_osc_listener('/restart', self.restart)
        Me.add_osc_listener('/detectstop', self.brother_detected)
        Me.add_osc_listener('/move', self.move)
        Me.add_osc_listener('/facemotion', self.facemotion)
        Me.add_osc_listener('/wander', self.wander)
        # for playing content
        Me.add_osc_listener('/servo', self.add_servo)
        Me.add_osc_listener('/omni', self.add_omni)
        Me.add_osc_listener('/voice', self.add_voice)
        Me.listenTCP()

    def init_processes(self):
        self.processes = {
            'compass': Process(
                target=read_compass,
                args=(Me,
                      self.shared_memory)),
            'camera': Process(
                target=gemini_camera,
                args=(1,  # exhibition pk
                      None,  # work pk -> 3
                      self.shared_memory,
                      )),
            'rssi': Process(
                target=get_rssi,
                args=(Me,
                      self.shared_memory,
                      self.shared_rssi),
                      ),
            'urg': Process(
                target=half_range_urg,
                args=(Me,
                      self.shared_memory, )),
            'omni': Process(
                target=omni_basic,
                args=(Me,
                      self.shared_memory)),
            'omni_controller': Process(
                target=omni_controller,
                args=(Me,
                      self.shared_memory, )),
            'afsm': Process(
                target=process_AFSM,
                args=(Me,
                      self.shared_memory,
                      self.shared_rssi)),
            'servo': Process(
                target=servo_controller,
                args=(Me,
                      self.shared_memory, )),
            'voice': Process(
                target=voice_controller,
                args=(Me,
                      self.shared_memory, )),
            }

    def main_loop(self):
        for name, p in self.processes.iteritems():
            p.start()
        self.handle_loop()

    def join_all(self):
        for name, p in self.processes.iteritems():
            p.join()
        del self.processes
