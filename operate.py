# -*- coding: utf-8 -*-
"""
operate.py
  実際に実行するためのファイル
"""
import sys
from utils.devices import Me


if __name__ == "__main__":
    if len(sys.argv) < 2:
        if Me == 'OPERATOR':
            # operator! -> commander
            #from behavior import commander
            print "[ERROR] you have to execute 'manage.py operate'"
        elif Me == 'GREEN' or Me == 'RED':
            print "[ERROR] you have to execute 'manage.py operate'"
            # gemini!
            sys.exit(1)
    else:
        # exec command if some string set.
        exec_path = sys.argv[1]
        __import__(exec_path)
