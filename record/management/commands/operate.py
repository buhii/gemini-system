# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from utils.devices import Me


class Command(BaseCommand):
    args = ''
    help = 'operate'

    def handle(self, *args, **options):
        # 実行
        if Me.role == 'OPERATOR':
            from behavior import commander
        else:
            from behavior.gemini import GeminiBehavior
            GeminiBehavior()
