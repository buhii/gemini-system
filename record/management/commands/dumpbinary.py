# -*- coding: utf-8 -*-
from datetime import datetime
from django.core.management.base import BaseCommand
from django.utils.timezone import get_current_timezone, make_aware
from record.models import CameraImage, URGDetailData


def get_integer_args(text, args=('year', 'month', 'day'), delimiter=' '):
    lst = map(int, filter(lambda v: v.strip(), raw_input(text).split(delimiter)))
    if lst >= len(args):
        return dict(zip(args, lst))
    else:
        raise ValueError("Invalid arguments!")


def dict2datetime(d):
    return datetime(year=d['year'], month=d['month'], day=d['day'], hour=d['hour'], minute=d['minute'])
d2d = dict2datetime


def _make_aware(d):
    return make_aware(d, get_current_timezone())


class Command(BaseCommand):
    args = ''
    help = 'operate'

    def handle(self, *args, **options):
        st, et = {}, {}   # start_time, end_time
        for _dict, text in ((st, 'starting'), (et, 'end')):
            _dict.update(get_integer_args("Please select %s date (YYYY MM DD): " % text))
            _dict.update(get_integer_args("Please select %s time (HH MM): " % text, ('hour', 'minute')))
        start_time, end_time = _make_aware(d2d(st)), _make_aware(d2d(et))
        if start_time > end_time:
            start_time, end_time = end_time, start_time

        print "[INFO] serializing models from %s to %s..." % (start_time, end_time)

        objects = CameraImage.objects.filter(created_at__range=(start_time, end_time))
        for o in objects:
            filename = "ci_%d_%s.jpg" % (o.pk, o.created_at)
            f = open(filename, 'wb')
            f.write(o.image)
            f.close()
            print "[INFO] writing %s ok" % filename

            filename = "ci_%d_%s.detected_area" % (o.pk, o.created_at)
            f = open(filename, 'wb')
            f.write(str(o.detect_area))
            f.close()
            print "[INFO] writing %s ok" % filename

        objects = URGDetailData.objects.filter(created_at__range=(start_time, end_time))
        for o in objects:
            filename = "ci_%d_%s.urg" % (o.pk, o.created_at)
            f = open(filename, 'wb')
            f.write(str(o.urg_array))
            f.close()
            print "[INFO] writing %s ok" % filename
