# -*- coding: utf-8 -*-
from datetime import datetime
from django.core.management.base import BaseCommand
from django.core import serializers
from django.utils.timezone import get_current_timezone, make_aware
from record.models import CameraImage, RSSIRecord, Compass, URGData, OmniControl, URGDetailData, FaceMotionData, AFSMData, RobotRecord


record_models = RobotRecord, RSSIRecord, Compass, URGData, OmniControl, FaceMotionData, AFSMData
#binary_models = CameraImage, URGDetailData


def get_integer_args(text, args=('year', 'month', 'day'), delimiter=' '):
    lst = map(int, filter(lambda v: v.strip(), raw_input(text).split(delimiter)))
    if lst >= len(args):
        return dict(zip(args, lst))
    else:
        raise ValueError("Invalid arguments!")


def dict2datetime(d):
    return datetime(year=d['year'], month=d['month'], day=d['day'], hour=d['hour'], minute=d['minute'])
d2d = dict2datetime


def _make_aware(d):
    return make_aware(d, get_current_timezone())


class Command(BaseCommand):
    args = ''
    help = 'operate'

    def handle(self, *args, **options):
        st, et = {}, {}   # start_time, end_time
        for _dict, text in ((st, 'starting'), (et, 'end')):
            _dict.update(get_integer_args("Please select %s date (YYYY MM DD): " % text))
            _dict.update(get_integer_args("Please select %s time (HH MM): " % text, ('hour', 'minute')))
        start_time, end_time = _make_aware(d2d(st)), _make_aware(d2d(et))
        if start_time > end_time:
            start_time, end_time = end_time, start_time

        print "[INFO] serializing models from %s to %s..." % (start_time, end_time)

        # serialize
        serializer = serializers.get_serializer("json")()

        for model in record_models:
            filename = "%s_%s_%s.json" % (start_time, end_time, model.__name__)
            print "[INFO] serialize to %s" % filename
            objects = model.objects.filter(created_at__range=(start_time, end_time))
            print "[INFO] got %s objects" % model.__name__
            data = serializer.serialize(objects, ensure_ascii=False)
            print "[INFO] serializing ok"
            f = open(filename, 'w')
            f.write(data)
            f.close()
            print "[INFO] writing %s ok" % filename
            print "[INFO] serialize %s completed." % filename
