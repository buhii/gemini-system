# -*- coding: utf-8 -*-
from django.utils import simplejson
from django.db import models
from utils.fields import BlobField


class RobotRecord(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=u"登録時刻")

    def save(self, *args, **kwargs):
        super(RobotRecord, self).save(*args, **kwargs)


class CameraImage(RobotRecord):
    """
    基底クラス
    """
    class Meta:
        verbose_name = u""
    image = BlobField(verbose_name=u'camera picture')
    _detect_area = models.CharField(max_length=255, verbose_name=u"detected area", default="", blank=True)

    def set_detect_area(self, results):
        self._detect_area = simplejson.dumps(results)

    def get_detect_area(self):
        return simplejson.loads(self._detect_area)
    detect_area = property(get_detect_area, set_detect_area)


class RSSIRecord(RobotRecord):
    source_addr = models.CharField(max_length=8, verbose_name=u"source address", default="", blank=True)
    rssi = models.IntegerField(verbose_name=u"rssi", default=0)


class Compass(RobotRecord):
    head = models.IntegerField(verbose_name=u"compass", default=0)


class URGData(RobotRecord):
    _array = models.CharField(max_length=63, verbose_name=u"area(divided by 22.5)", default="", blank=True)

    def set_array(self, result):
        self._array = ','.join(map(str, result))

    def get_array(self):
        return map(int, self._array.split(','))
    array = property(get_array, set_array)


class OmniControl(RobotRecord):
    direction = models.PositiveIntegerField(default=0)
    speed = models.PositiveIntegerField(default=0)


class URGDetailData(RobotRecord):
    urg_array = BlobField(verbose_name=u"urg detail data")


class FaceMotionData(RobotRecord):
    is_automatic = models.BooleanField(verbose_name=u"自動的に付与されたか？", default=False)
    pitch = models.PositiveIntegerField(verbose_name=u"pitch", default=7500)
    yaw = models.PositiveIntegerField(verbose_name=u"yaw", default=7500)


class AFSMData(RobotRecord):
    status = models.IntegerField(verbose_name=u"afsm status", default=0)

class ConversationData(RobotRecord):
    title = models.CharField(max_length=255, verbose_name=u"会話タイトル", default="", blank=True)
    min_distance = models.PositiveIntegerField(default=0)
    min_role = models.CharField(max_length=8, verbose_name=u"source address", default="", blank=True)
