gemini-system
=============

Some code stuff for museum guide robot gemini.


Requirements
============
* BeautifulSoup4
* Django
* MySQL-python
* PIL
* PyYAML
* South
* Twisted
* Xbee
* django-celery
* numpy
* pyferns (https://github.com/buhii/pyferns/)
* pyliburg (https://github.com/buhii/pyliburg/)
* pygame
* redis
* txosc
