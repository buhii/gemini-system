# -*- coding: utf-8 -*-
import chardet
from bs4 import BeautifulSoup
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import HttpResponseRedirect, render_to_response
from django.template import RequestContext
from museum.models import ServoMotion
from museum.forms import XMLForm


def add_by_xml(request):
    validation_form = XMLForm

    if request.method == "POST":
        xml_form = validation_form(request.POST, request.FILES)
    else:
        xml_form = validation_form()

    if xml_form.is_valid():
        # 登録する
        performer = request.POST['performer']
        print performer
        f = request.FILES['file']
        data = f.read()
        encoding = chardet.detect(data)['encoding']
        xml_doc = data.decode(encoding, 'ignore')
        soup = BeautifulSoup(xml_doc, 'xml')

        for activity in soup.find_all('anyType'):
            if activity.get('xsi:type') != u'ActivityData':
                continue
            name=activity.Text.text
            code=activity.ProgramCode.anyType.text

            # すでに同じ名前で登録されていないか調査する
            try:
                servo = ServoMotion.objects.get(name=name, code=code, performer=performer)
            except ServoMotion.DoesNotExist:
                servo = ServoMotion(performer=performer, name=name, code=code)
            servo.save()

        return HttpResponseRedirect("/museum/servomotion/")


    return render_to_response(
        'admin/museum/servomotion/add_by_xml.html',
        {
            'xmlform': xml_form,
            },
        context_instance=RequestContext(request))
add_by_xml = staff_member_required(add_by_xml)
