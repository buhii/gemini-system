# -*- coding: utf-8 -*-
from math import sqrt, atan2, degrees, sin, cos
from numpy import array, dot, identity
from numpy.linalg import norm
from struct import unpack
from utils.commons import RingBuffer, average, AccelerometerAverage

F_CPU = int(8.04 * 1000 * 1000)
G = 9.7976713

def ts2time(ts):
    # calculate timestamp(16bits) to time scale(in sec)
    return ts / (F_CPU / 64.0)

DEBUG = False


format2length = {
    'c': 1,
    'b': 1,
    'h': 2,
    'l': 4,
    'i': 4,
    'f': 4,
    'd': 8,
}
def calc_format_length(packed_format):
    total_length = 0
    for char in packed_format[1:]:
        total_length += format2length[char.lower()]
    return total_length


class VSAS(object):
    DELIMITER_CODE = "\x10\x02\xff"
    ATTRIBUTES_2GM = (
        ('status', '>H', None),
        ('counter', '>H', None),
        ('yaw_pitch_roll', '>hhh', 2 ** -13),
        ('gyr_raw', '>hhh', 2 ** -12),
        ('acc_raw', '>hhh', 2 ** -8),

        ('gps_no_input_time', '>H', None),
        ('calc_latitude', '>l', 2 ** -29),
        ('calc_longitude', '>l', 2 ** -29),
        ('gps_data', '>lll', 2 ** -29),

        ('velocity_raw', '>hhh', 2 ** -6),
        ('gps_num_satelites', '>c', None),
        ('gps_status', '>c', None),

        ('gps_direction', '>h' ,2 ** -13),
        ('gps_velocity', '>H', 2 ** -6),
        ('gps_hdop', '>H', 2 ** -8),
        ('mag_output', '>h', 2 ** -13),

        ('utc_time', '>L', None),
        ('utc_day', '>L', None),
    )
    # red's compass parameter
    mag_max = map(float, [294, 312, 647])
    mag_min = map(float, [-339, -332, -767])

    def __init__(self, serial_obj):
        self.device = serial_obj
        self.acc_avg_calc = AccelerometerAverage()
        self.old_timestamp = 0
        self.acc_diff = array([0] * 3)
        self.scale = 2.0
        self.head_buf = RingBuffer(boundary_size=4)
        self.moving_buf = RingBuffer(buf_size=4)

    def calc_average_start(self):
        self.acc_avg_calc.start()

    def calc_average_stop(self):
        self.acc_avg_calc.stop()
        self.acc_diff = self.acc_avg_calc.get_average()
        print "acc_diff:", self.acc_diff

    @property
    def is_calculating_average(self):
        return self.acc_avg_calc.is_running

    def get_acceleration(self):
        # return actual acceleration (m/sec^2)
        #return self.acc * G

        # consider body orientation
        a_x, a_y, a_z = self.acc_raw
        acc_raw_transpose = array([[a_x, a_y, a_z]]).T

        rmatrix = self.calc_rotate_matrix()

        dotted = dot(rmatrix, acc_raw_transpose)
        t = array([dotted[0][0], dotted[1][0], dotted[2][0]]) # + G])
        return t

    def get_passed_time(self):
        # return delta time (sec)
        #return float(self.timestamp_diff * 64) / F_CPU
        return 0.01   # 10ms

    def is_delimiter(self):
        for char in self.DELIMITER_CODE:
            if self.device.read() != char:
                return False
        return True

    def wait_until_delimiter(self):
        try:
            while True:
                if self.is_delimiter():
                    break
        except KeyboardInterrupt:
            raise KeyboardInterrupt

    def read_attributes(self):
        result = {}
        total_output = ""
        self.wait_until_delimiter()
        for attr, packed_format, lsb_multiplier in self.ATTRIBUTES_2GM:
            tmp = self.device.read(calc_format_length(packed_format))
            total_output += tmp
            unpacked = unpack(packed_format, tmp)

            if lsb_multiplier:
                unpacked = map(lambda v: v * lsb_multiplier, unpacked)

            if packed_format == 2:   # '>?'
                result[attr] = unpacked[0]
            else:
                result[attr] = unpacked

        # DLE, ETX の確認
        dle_etx = self.device.read(2)
        try:
            assert dle_etx == '\x10\x03'
        except AssertionError:
            return None

        # BCC (チェックサム) の確認
        checksum = chr(sum(map(ord, total_output + dle_etx[1])) % 0x100)
        bcc = self.device.read(1)
        try:
            assert checksum == bcc
        except AssertionError:
            print "[NG] calculated checksum: 0x%02x, BCC: 0x%02x" % (ord(checksum), ord(bcc))
            return None

        return result

    def read(self):
        result = None
        while not result:
            try:
                result = self.read_attributes()
            except KeyboardInterrupt:
                raise KeyboardInterrupt

        for attr, value in result.iteritems():
            setattr(self, attr, value)
        #self.yaw_pitch_roll[2] = -self.yaw_pitch_roll[2]  # ???

        for attr in ('status',
                     'counter'):
            if DEBUG:
                print '[DEBUG] %s: %s' % (attr, getattr(self, attr))

        # calculate compensated acceleration with considering orientation
        #self.acc_compensated = self.acc_raw
        self.acc_compensated = self.get_acceleration()
        if DEBUG:
            print "yaw pitch roll: ", self.yaw_pitch_roll
            print "acc_raw       : ", self.acc_raw
            print "acc_compensated:", self.acc_compensated

        # for calculation
        if self.is_calculating_average:
            self.acc_avg_calc.add(self.acc_compensated)
            print "gyr_norm       : ", norm(self.gyr_raw)

    def calc_rotate_matrix(self):
        alpha, beta, gamma = map(lambda v: v, self.yaw_pitch_roll)
        r_alpha = array(
            [[ cos(alpha), -sin(alpha),           0],
             [ sin(alpha),  cos(alpha),           0],
             [          0,           0,           1]])
        r_beta = array(
            [[  cos(beta),           0,   sin(beta)],
             [          0,           1,           0],
             [ -sin(beta),           0,   cos(beta)]])
        r_gamma = array(
            [[          1,           0,           0],
             [          0,  cos(gamma), -sin(gamma)],
             [          0,  sin(gamma),  cos(gamma)]])
        #return dot(dot(r_alpha, r_beta), r_gamma)
        return dot(r_beta, r_gamma)

        """
        A, B, G = alpha, beta, gamma
        return array([
            [cos(A)*cos(B), cos(A)*sin(B)*sin(G)-sin(A)*cos(G), cos(A)*sin(B)*cos(G)+sin(A)*sin(G)],
            [sin(A)*cos(B), sin(A)*sin(B)*sin(G)+cos(A)*cos(G), sin(A)*sin(B)*cos(G)-cos(A)*sin(G)],
            [      -sin(B),                      cos(B)*sin(G),                      cos(B)*cos(G)]])
        """
