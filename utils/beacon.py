# -*- coding: utf-8 -*-
"""
beacon.py
  some useful stuff for XBee
"""
from struct import unpack
import time
from utils.devices import Me
from utils.commons import assign_shared_memory
from xbee import XBee
from record.models import RSSIRecord
from gemini_system.universal import RSSI_MIN
from txosc import osc


RSSI_DEBUG = False


def calc_rssi(char):
    return -ord(char)


class XBeeResponse(object):
    @classmethod
    def get_source_addr(cls, response):
        return hex(unpack('>H', response['source_addr'])[0])

    @classmethod
    def get_rssi(cls, response):
        return calc_rssi(response['rssi'])

    def __init__(self, response):
        self.response = response
        self.source_addr = self.get_source_addr(response)
        self.rssi = self.get_rssi(response)


def get_response(coordinator):
    while True:
        try:
            response = coordinator.wait_read_frame()
            return XBeeResponse(response)
        except KeyboardInterrupt:
            raise KeyboardInterrupt


try:
    coordinator = XBee(Me.xbee)
except Exception as e:
    print e
    coordinator = object

def get_rssi(me, shared_memory, shared_rssi):
    """
    get rssi and send with osc.Message
    """
    while True:
        target_addr = shared_rssi['target_addr']
        try:
            response = get_response(coordinator)
        except KeyboardInterrupt:
            raise KeyboardInterrupt

        # 結果が帰ってきたので時刻を更新
        shared_rssi['latest_get_time'] = time.time()

        if shared_memory['is_record'] == True:
            # DB に格納
            ri = RSSIRecord(rssi=response.rssi, source_addr=response.source_addr)
            ri.save()

        # 最も強いビーコンを調べる
        xbee_list = sorted(shared_rssi['data'].items(),
                           key=lambda tpl: -tpl[1])
        if xbee_list:
            max_addr, max_rssi = xbee_list[0]
        else:
            max_addr, max_rssi = None, RSSI_MIN

        # 共有メモリに格納
        def update_dict(d): d[response.source_addr] = response.rssi
        assign_shared_memory(shared_rssi, 'data', update_dict)

        def update_ring_buffer(b): b.append(max_addr)
        assign_shared_memory(shared_rssi, 'max_addr_list', update_ring_buffer)

        if RSSI_DEBUG:
            print '[RSSI] target: %s - data: %s' % (shared_rssi['target_addr'], shared_rssi['data'])
            print '[RSSI] most appeared: %s' % shared_rssi['max_addr_list'].most_appeared()
            print '[RSSI] buffer %s' % shared_rssi['max_addr_list']
            print '[RSSI] SENTINELS: 0x1005=%s, 0x1006=%s' % (
                shared_rssi['data'].get('0x1005'),
                shared_rssi['data'].get('0x1006'),)

        # あんまり良くないが録画中のみ送信する
        if shared_memory['is_record'] == True:
            if response.source_addr == target_addr:
                # 目標ビーコンの値を取得することができた
                msg = osc.Message('/rssi')
                msg.add(response.source_addr)
                msg.add(response.rssi)
                me.send(me.operator, msg)
            elif response.source_addr == me.brother_xbee_id:
                msg = osc.Message('/rssi_bro')
                msg.add(response.source_addr)
                msg.add(response.rssi)
                me.send(me.operator, msg)
        time.sleep(0.02)
