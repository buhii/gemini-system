# -*- coding: utf-8 -*-
from django.db import models
from django.db.models import FileField
from django.forms import forms
from django.template.defaultfilters import filesizeformat
from django.utils.translation import ugettext_lazy as _

class BlobField(models.TextField):
    """
    Django には BlobField が用意されていないため作成
    See: http://djangosnippets.org/snippets/1495/
    """
    def get_internal_type(self):
        return "TextField"

    def db_type(self, connection):
        from django.conf import settings
        db_types = {
            'django.db.backends.mysql':'longblob',
            'django.db.backends.sqlite3':'blob'
            }
        try:
            return db_types[settings.DATABASES['default']['ENGINE']]
        except KeyError:
            raise Exception, '%s currently works only with: %s' % (
                self.__class__.__name__,','.join(db_types.keys()))

from south.modelsinspector import add_introspection_rules
add_introspection_rules([
     (
         [BlobField], # Class(es) these apply to
         [],         # Positional arguments (not used)
         {           # Keyword argument
         },
     ),
 ], ["^utils\.fields\.BlobField"])


# http://djangosnippets.org/snippets/2206/
class ContentTypeRestrictedFileField(FileField):
    """
    Same as FileField, but you can specify:
        * content_types - list containing allowed content_types. Example: ['application/pdf', 'image/jpeg']
        * max_upload_size - a number indicating the maximum file size allowed for upload.
            2.5MB - 2621440
            5MB - 5242880
            10MB - 10485760
            20MB - 20971520
            50MB - 5242880
            100MB 104857600
            250MB - 214958080
            500MB - 429916160
    """
    def __init__(self, *args, **kwargs):
        self.content_types = kwargs.pop("content_types")
        self.max_upload_size = kwargs.pop("max_upload_size")

        super(ContentTypeRestrictedFileField, self).__init__(*args, **kwargs)

    def clean(self, *args, **kwargs):
        data = super(ContentTypeRestrictedFileField, self).clean(*args, **kwargs)

        file = data.file
        try:
            content_type = file.content_type
            if content_type in self.content_types:
                if file._size > self.max_upload_size:
                    raise forms.ValidationError(_('Please keep filesize under %s. Current filesize %s') % (filesizeformat(self.max_upload_size), filesizeformat(file._size)))
            else:
                raise forms.ValidationError(u'対応していないファイルの種類です。')
        except AttributeError:
            pass

        return data
