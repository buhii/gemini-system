# -*- coding: utf-8 -*-
import os.path
import yaml
from gemini_system.universal import LOCAL_OPERATION_DIR


class ExhibitionYaml(object):
    def __init__(self, pk):
        yml_path = os.path.join(
            LOCAL_OPERATION_DIR,
            "exhibition_%d.yml" % pk)
        self.works = yaml.load(open(yml_path).read())

    def __getitem__(self, index):
        return self.works[index]
