# -*- coding: utf-8 -*-
import cv2
from numpy import array, int32
from utils.commons import average

def variance(ary):
    _avg = average(ary)
    return sum(map(lambda v: (_avg - v) ** 2, ary)) / float(len(ary))


def norm(ary):
    _avg = average(ary)
    return map(lambda v: v / _avg, ary)


def get_contour(v):
    return array((
            ((v[0], v[1]), ),
            ((v[2], v[3]), ),
            ((v[4], v[5]), ),
            ((v[6], v[7]), )
            ),
            dtype=int32)


def is_contour_convex(v):
    """
    return point array if contour is convex or None
    """
    contour = get_contour(v)
    if cv2.isContourConvex(contour):
        return v
    else:
        return None
