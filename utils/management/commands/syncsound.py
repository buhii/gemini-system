# -*- coding: utf-8 -*-
import os
import time
import urllib2
from django.core.management.base import BaseCommand
from museum.models import Action


def _print(text):
    print text
    time.sleep(0.01)


def is_regular_file(sfpath):
    try:
        v = open(sfpath)
    except IOError:
        _print(u"[INFO] No File exist. Get new one.")
        return False
    sound_file = v.read()
    v.close()
    if len(sound_file) > 0:
        _print(u"[INFO] %s: file existed. skip." % sfpath)
        return True
    else:
        _print(u"[INFO] %s: file is empty. Retry." % sfpath)
        return False


def prepare_dir(soundpath):
    try:
        os.makedirs(os.path.split(soundpath)[0])
    except OSError:
        pass


def save_soundfile(sf):
    # Django の Admin 画面にログインし、音声ファイルをダウンロードする
    auth_handler = urllib2.HTTPBasicAuthHandler()
    # FIXME: 固定になってしまっているのを修正する
    auth_handler.add_password(None,
                              uri='http://www3394ue.sakura.ne.jp/',
                              user='gemini',
                              passwd='4211',
                              )
    opener = urllib2.build_opener(auth_handler)
    urllib2.install_opener(opener)
    sound_url = urllib2.urlopen(sf.sound.url)
    sound_data = sound_url.read()
    _print("[INFO] %s(%s) downloaded from %s." % (
            sf.local_sound_path(),
            len(sound_data),
            sf.sound.url))
    f = open(sf.local_sound_path(), 'w')
    f.write(sound_data)
    f.close()


class Command(BaseCommand):
    args = ''
    help = 'syncsound'

    def handle(self, *args, **options):
        print "\nsyncsound."
        for a in Action.objects.all():
            if not a.sound:
                continue
            prepare_dir(a.local_sound_path())
            while not is_regular_file(a.local_sound_path()):
                save_soundfile(a)
            _print("[INFO] sound %s downloading completed." % a.local_sound_path())
