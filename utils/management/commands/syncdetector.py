# -*- coding: utf-8 -*-
import hashlib
import sys
import time
from django.core.management.base import BaseCommand
from museum.models import Figure, get_local_doi_path


def _print(text):
    print text
    time.sleep(0.1)


def is_regular_file(f):
    try:
        v = open(f.local_doi_path())
    except IOError:
        _print(u"[INFO] No File exist. Get new one.")
        return False
    doi_file = v.read()
    v.close()
    test_hash = hashlib.sha224(doi_file).hexdigest()
    if test_hash == f.checksum:
        _print(u"[INFO] %s: checksum ok. skip." % f.local_doi_path())
        return True
    else:
        _print(u"[ERROR] %s: checksum NG! (%s != %s)Retry." % (
                f.local_doi_path(), test_hash, f.checksum))
        return False


class FakeFigure(object):
    def __init__(self, _dict):
        for attr in ('pk', 'work__pk', 'checksum', 'status'):
            setattr(self, attr, _dict[attr])

    def get_doi(self):
        _print("[INFO] downloading %s" % self.local_doi_path())
        f_doi = Figure.objects.filter(pk=self.pk).values('doi')[0]['doi']
        _print("[INFO] %d bytes. " % len(f_doi))
        return f_doi

    def local_doi_path(self):
        return get_local_doi_path(self.pk, self.work__pk)


def save_doi_file(f):
    doi_file = open(f.local_doi_path(), 'w')
    _print("[INFO] open %s" % f.local_doi_path())
    doi_file.write(f.get_doi())
    doi_file.close()
    _print("[INFO] file closed.")


def save_until_successful(f):
    result = False
    _print("[INFO] download start. @ %s" % f.local_doi_path())
    while not result:
        try:
            save_doi_file(f)
            # check hash value
            v = open(f.local_doi_path())
            test_hash = hashlib.sha224(v.read()).hexdigest()
            result = test_hash == f.checksum
            v.close()
            if not result:
                _print("[ERROR] checksum not valid! (%s != %s)" % (
                        test_hash, f.checksum))
        except KeyboardInterrupt:
            sys.exit()


class Command(BaseCommand):
    args = ''
    help = 'syncdetectors'

    def handle(self, *args, **options):
        # 検出器をローカルに保存する
        _print("\nsyncdetector.")
        for f_dict in Figure.objects.values('pk', 'work__pk', 'checksum', 'status'):
            f = FakeFigure(f_dict)
            if f.status != Figure.STATUS_COMPLETE:
                _print("[INFO] figure status is not complete: skip.")
                continue

            if is_regular_file(f):
                continue
            save_until_successful(f)
            _print("[INFO] figure %s downloading completed." % f.local_doi_path())
