# -*- coding: utf-8 -*-
import sys
from optparse import make_option
from museum.models import Work, Figure, Exhibition, Action
import yaml
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--exhibition',
                    action='store',
                    dest='exhibition',
                    default=None,
                    help='generate with this exhibition pk settings.'
                    ), )
    args = ''
    help = 'sync scenario'

    def handle(self, *args, **options):
        print "\nsyncscenario."
        if not options['exhibition']:
            print "Usage: python manage.py syncscenario --exhibition [e]\n"
        else:
            try:
                exhibition = Exhibition.objects.get(pk=options['exhibition'])
            except Exhibition.DoesNotExist:
                print "Cannot find exhibition(pk=%s)" % options['exhibition']
                sys.exit()

            # serialize work objects
            print "[INFO] getting json file @ exhibition '%s'" % exhibition
            works = Work.objects.filter(exhibition=exhibition, is_valid=True).values(
                'pk', 'name', 'scenario', 'beacon__source_addr', 'beacon__rssi_threshold', 'exhibition',
                )
            for work in works:
                work['figures'] = list(Figure.objects.filter(work=work['pk']).values_list('pk', flat=True))
                work['actions'] = list(Action.objects.filter(work=work['pk']).values(
                        'priority',
                        'performer',
                        'timespan',
                        'sound',
                        'servo__code',
                        'omni_direction',
                        'omni_timespan',
                        ))
            yaml_data = yaml.dump(list(works))

            f_sce = open(exhibition.get_scenario_path(), 'w')
            f_sce.write(yaml_data)
            f_sce.close()
            print "[INFO] wrote exhibition scenario file '%s' @ %d(bytes)" % (
                exhibition.get_scenario_path(), len(yaml_data))
