# -*- coding: utf-8 -*-
import time
from random import random
from utils.afsm import AFSM
try:
    from record.models import OmniControl
except ImportError:
    print "[WARNING] not running from manage.py: Can't import record.models!"

MAX_LENGTH = 4000
EMERGENCY_STOP_LENGTH = 50  # 200
LEFT_LENGTH = 600 # 800
OVER_LENGTH = 480 # 580
DEBUG_F = False


def out_command(cmd, param):
    cmd = cmd.replace('x', str(param))
    cmd += '\x0d'
    return cmd


def omniwheel_stop(me, is_record=False):
    me.omniwheel.write(out_command("00000000", 0))
    # write to DB
    if is_record == True:
        try:
            tmp = OmniControl(direction=0, speed=0)
            tmp.save()
        except Exception as e:
            print "[ERROR] %s" % e


class OmniwheelControl(object):
    STOP = X = 0
    L = 1
    R = 2
    F = 3
    B = 4
    dir_command = {
        X: '00000000',
        L: '1111xxx0',
        R: '0000xxx0',
        F: '1000xx00',
        B: '0100xx00',
        }
    def __init__(self, direction, speed=3, memo=None):
        self.direction = direction
        self.speed = speed
        self.memo = memo

    def execute(self, me, is_record):
        if self.memo and DEBUG_F: print self.memo, self.direction
        if self.direction in (self.L, self.R):
            speed = 1
        else:
            speed = self.speed
        me.omniwheel.write(out_command(self.dir_command[self.direction], speed))
        # write to DB
        if is_record == True:
            tmp = OmniControl(direction=self.direction, speed=self.speed)
            tmp.save()
OC = OmniwheelControl


def random_walk(me, is_record):
    # if random walk successful, return True
    t = random()
    if t < 0.004:
        print "random walk"
        if random() < 0.5:
            oc = OC(OC.L, 2, "random walk: L")
        else:
            oc = OC(OC.R, 2, "random walk: R")
        oc.execute(me, is_record)
        time.sleep(4.0)
        return True
    return False


def is_emergency_length(ary):
    return bool(filter(lambda v: v < EMERGENCY_STOP_LENGTH + 100, ary))   # 少し長めで


def turn_to(me, shared_memory):
    # コンパスの値を用いて、ある特定の方向まで回転する
    compass_dir = shared_memory['compass_dir'] % 360 + 360
    def get_turn_diff():
        return abs(compass_dir - shared_memory['compass']) % 360
    is_record = shared_memory['is_record']
    tf = get_turn_diff()
    if tf < 45:
        return
    elif tf > 180:
        direction = OC.R
    else:
        direction = OC.L
    while get_turn_diff() > 45.0:
        # ガードタイムを更新する
        shared_memory['target_guard_time'] = time.time() + 1.0
        OC(direction, 1).execute(me, is_record)
        time.sleep(0.1)


def omni_basic(me, shared_memory):
    """
    vibration control subsume 'avoid and wander' function
    """
    def force_same_rotation(old_oc):
        print "force!!!!!!!!"
        old_oc.execute(me, is_record)
        time.sleep(4.0)
        if shared_memory['AFSM_STATUS'] == AFSM.WANDER:
            shared_memory['AFSM_STATUS'] = AFSM.VIBRATION_AVOID
        print "force end!!!!!!!!"

    old_direction = 0
    old_speed = 0

    while True:
        time.sleep(0.025)
        status = shared_memory['AFSM_STATUS']
        is_record = shared_memory['is_record']

        if status == AFSM.STOP or status == AFSM.LOADING or status == AFSM.DETECT:
            """
            Stop.
              絵が見つかったり、構成中だったり、初期状態のとき等
            """
            omniwheel_stop(me, is_record)
            continue

        elif status == AFSM.TURN_TO:
            # コンパスの値を用いて、ある特定の方向まで回転する
            turn_to(me, shared_memory)
            if shared_memory['AFSM_STATUS'] == AFSM.TURN_TO:
                #shared_memory['AFSM_STATUS'] = AFSM.WANDER
                shared_memory['AFSM_STATUS'] = AFSM.STOP
            continue

        elif status == AFSM.WAIT:
            omniwheel_stop(me, is_record)
            continue

        elif status == AFSM.DIRECTION_L:
            OC(OC.L, 1).execute(me, is_record)
            continue

        elif status == AFSM.DIRECTION_R:
            OC(OC.R, 1).execute(me, is_record)
            continue

        elif status == AFSM.DIRECTION_F:
            if is_emergency_length(shared_memory['urg_data_8div']):
                OC(OC.STOP, 0, "emergency stop!").execute(me, is_record)
            else:
                OC(OC.F, 3).execute(me, is_record)
            continue

        elif status == AFSM.DIRECTION_B:
            OC(OC.B, 3).execute(me, is_record)
            continue

        elif status == AFSM.FOUND_XBEE:
            """
            FOUND_XBEE
              XBEE が見つかったとき
              一周してみて、絵がないか探してみる
            """
            print "AFSM_FOUND_XBEE!!!!!!!" * 5
            if old_direction not in (OC.L, OC.R):
                old_direction = OC.L
            OC(old_direction, old_speed).execute(me, is_record)
            time.sleep(6.0)
            if shared_memory['AFSM_STATUS'] == AFSM.FOUND_XBEE:
                shared_memory['AFSM_STATUS'] = AFSM.WANDER

        elif status not in (AFSM.WANDER, AFSM.LOADED):
            print "elif status!", status
            continue

        else:
            """
            それ以外の時
            うろうろする
            """
            oc = avoid_and_wander(shared_memory, me)
            # suppress vibration
            if (old_direction == OC.L and oc.direction == OC.R) or \
               (old_direction == OC.R and oc.direction == OC.L):
                force_same_rotation(OC(old_direction, old_speed))
                #old_direction = OC.X   # STOP
                old_speed = old_speed
            else:
                old_direction = oc.direction
                old_speed = oc.speed
            try:
                oc.execute(me, is_record)
            except KeyboardInterrupt:
                omniwheel_stop(me, is_record)
                raise KeyboardInterrupt
            time.sleep(0.01)
            shared_memory['AFSM_STATUS'] = AFSM.WANDER   # これで更新されるだろうか...
            time.sleep(0.01)


def remove_error_data(urg_data):
    return filter(lambda t: t > 20, urg_data)


def get_min_length(data):
    v = remove_error_data(data)
    if v:
        return min(v)
    else:
        return MAX_LENGTH


def avoid_and_wander(shared_memory, me):
    """avoid + wander AFSM"""
    left_index = []
    over_index = []
    speed = 3
    for i in range(8):
        d = shared_memory['urg_data_8div'][i]
        if d < EMERGENCY_STOP_LENGTH:
            print "emergency stop!"
            return OC(OC.STOP, 0, "emergency stop!")
        if d > LEFT_LENGTH:
            left_index.append(i)
        elif d < OVER_LENGTH:
            over_index.append(i)

    left_index = set(left_index)
    over_index = set(over_index)
    # if DEBUG_F: print "left_index", left_index
    # if DEBUG_F: print "over_index", over_index

    if len(over_index) == 8:
        shared_memory['straight_count'] = 0
        return OC(OC.L, speed, "No space left to forward")

    elif len(over_index.intersection((0, 1))) > 0:
        shared_memory['straight_count'] = 0
        return OC(OC.L, speed, "Over: L")

    elif len(over_index.intersection((6, 7))) > 0:
        shared_memory['straight_count'] = 0
        return OC(OC.R, speed, "Over: R")

    elif left_index.issuperset((3, 4)):
        shared_memory['straight_count'] += 1
        if shared_memory['straight_count'] > 200:
            if random_walk(me, shared_memory['is_record']):
                shared_memory['straight_count'] = 0
                shared_memory['random_walk'] = time.time()
        return OC(OC.F, speed, "Forward")

    elif (not len(left_index.intersection((0, 1, 2))) > 1) and \
              len(left_index.intersection((5, 6, 7))) > 1:
        shared_memory['straight_count'] = 0
        return OC(OC.L, speed, "Left: L")
    elif len(left_index.intersection((0, 1, 2))) > 1:
        shared_memory['straight_count'] = 0
        return OC(OC.R, speed, "Left: R")

    else:
        shared_memory['straight_count'] = 0
        return OC(OC.L, speed, "???: L")
