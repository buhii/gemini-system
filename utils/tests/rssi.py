# -*- coding: utf-8 -*-
import sys
import time
from datetime import datetime
import serial
from struct import unpack
from xbee import XBee
from utils.beacon import calc_rssi


result_dict = {}
def print_source():
    result = []
    for key, tpl in result_dict.iteritems():
        result.append((key, tpl[1]))
    print sorted(result, key=lambda tpl: -tpl[1])


def display_response(response):
    source_addr = hex(unpack('>H', response['source_addr'])[0])
    if source_addr == '0x1234': return
    rssi = calc_rssi(response['rssi'])
    #source_addr = hex(unpack('>H', response['source_addr'])[0])
    result_dict[source_addr] = (datetime.now(), rssi)
    if rssi > -128 and source_addr != '0x1234':
        print_source()
        """
        print "source addr: %s\trssi: %d\t%s" % (
            source_addr,
            rssi,
            datetime.now(),
            )
        """

def get_frame_until_rx_io_data(coordinator):
    # read frames
    while True:
        try:
            response = coordinator.wait_read_frame()
            display_response(response)
            return (calc_rssi(response['rssi']),
                    map(lambda d: d['dio-1'], response['samples']))
        except KeyboardInterrupt:
            print result_dict
            sys.exit()


if __name__ == '__main__' or 'utils.tests.rssi':
    if len(sys.argv) >= 3:
        mode = int(sys.argv[2])
    else:
        mode = 0
    if mode == 0:
        coordinator_serial =  serial.Serial('/dev/tty.usbserial-A100RU7F', 9600)
    else:
        coordinator_serial =  serial.Serial('/dev/tty.usbserial-A40081CZ', 9600)

    coordinator = XBee(coordinator_serial)
    while True:
        try:
            get_frame_until_rx_io_data(coordinator)
            time.sleep(0.02)
        except KeyboardInterrupt:
            print result_dict
            sys.exit()
