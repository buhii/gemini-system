# -*- coding: utf-8 -*-
import time
import zlib
import serial

from struct import pack, unpack
from collections import Counter
from math import sin, cos, atan2, degrees
from itertools import groupby
from numpy import array
try:
    from txosc import osc, async
except:
    pass


average = lambda ary: sum(ary) / float(len(ary))
MAX_LENGTH = 4000


class TimeLimitedClosingSerial(serial.Serial):
    def __init__(self, *args, **kwargs):
        super(TimeLimitedClosingSerial, self).__init__(*args, **kwargs)

    def close(self, *args, **kwargs):
        # time limited closing
        pass


def insert_top_arg(msg, obj):
    arguments = msg.arguments
    arguments.insert(0, osc.StringArgument(obj))
    m = osc.Message(msg.address)
    for arg in arguments:
        m.add(arg)
    return m


def remove_error_data(urg_data):
    return filter(lambda t: t > 20, urg_data)


def get_min_length(data):
    v = remove_error_data(data)
    if v:
        return min(v)
    else:
        return MAX_LENGTH


def convert_servo_command(s):
    return ''.join(map(lambda v: chr(int(v, 16)), s.split(' ')))


def human_readable_hex(s):
    return ' '.join(map(lambda c: '%02x' % ord(c), s))


def assign_shared_memory(shared, attr, func):
    # shared_memory の DictProxy は reassign する必要があるために用意した関数
    obj = shared[attr]
    func(obj)
    shared[attr] = obj


def split_len(seq, length):
    # See: http://code.activestate.com/recipes/496784
    return [seq[i:i+length] for i in range(0, len(seq), length)]

def list_operate(func, *args):
    result = []
    for vals in zip(*args):
        result.append(func(*vals))
    return result


def infinite_loop(f):
    def loop_func(*args, **kwargs):
        while True:
            f(*args, **kwargs)
    return loop_func


class AccelerometerAverage(object):
    def __init__(self):
        self.is_running = False
        self.count = 0
        self.sum = array([0.0] * 3)

    def add(self, v):
        self.sum += v
        print "acc_sum: ", self.sum
        self.count += 1

    def start(self):
        self.is_running = True
        self.count = 0
        self.sum = array([0.0] * 3)

    def stop(self):
        self.is_running = False

    def get_average(self):
        return self.sum / float(self.count)


class RingBuffer(object):
    """
    See: http://www.saltycrane.com/blog/2007/11/python-circular-buffer/
    """
    def __init__(self, buf_size=8, boundary_size=1, default=0):
        self.buf = [default] * buf_size
        self.buf_size = buf_size
        self.boundary_size = boundary_size

    def __str__(self):
        return "<RingBuffer: %s>" % repr(self.buf)

    def average(self):
        return average(filter(lambda v: v != None, self.buf))

    def append(self, value):
        self.buf.pop(0)
        self.buf.append(value)

    def get(self):
        return self.buf

    def median_filter(self):
        bs = self.boundary_size
        ret = []
        buf = self.buf[:]
        buf_start = buf[0]
        buf_end = buf[-1]
        buf = [buf_start] * bs + buf + [buf_end] * bs
        for i in range(bs, self.buf_size + bs):
            median = sorted(buf[i - bs: i + bs + 1])[bs]
            ret.append(median)
        return ret

    def most_appeared(self):
        ct = Counter()
        for val in self.buf:
            ct[val] += 1
        ct = filter(lambda p: p[0] != None, sorted(ct.items(), key=lambda tpl: -tpl[1]))
        if len(ct):
            return ct[0][0]


def polar2cartesian(r, theta):
    return r * cos(theta), r * sin(theta)


class Point(object):
    def __init__(self, *args):
        self.x, self.y = args

    def calc_inclination(self, other):
        return degrees(atan2(self.y - other.y, self.x - other.x))


def complement_true_false_array(ary, max_false_length=3):
    """
    (True, False, True, False, False, False, True)
     => (True, True, True, False, False, False, True)
    """
    ret = []
    for v, _iter in groupby(ary):
        group = list(_iter)
        if len(group) <= max_false_length:
            ret.extend([True] * len(group))
        else:
            ret.extend(group)
    return ret


diff = lambda a, b: abs(a - b)


def calc_front_wall(cartesian_array):
    len_array = len(cartesian_array)
    point_array = map(lambda tpl: Point(*tpl), cartesian_array)

    # calc inclinations
    inclinations = []
    for i, p in enumerate(point_array):
        pickup_point = p
        pickup_point_next = point_array[(i + 3) % len_array]
        inclinations.append(pickup_point_next.calc_inclination(pickup_point))

    # find wall-like points
    tmp_wall_start = 0
    walls = []
    while tmp_wall_start < (len_array - 1):
        pickup_inclination = inclinations[tmp_wall_start]
        is_the_same_inclinations = map(
            lambda d: abs(pickup_inclination - d) < 10,   # within ±10 degree
            inclinations[tmp_wall_start + 1:])
        is_the_same_inclinations = complement_true_false_array(is_the_same_inclinations, 10)

        if not is_the_same_inclinations[0]:   # no hit...
            tmp_wall_start += 1
            continue

        tmp_wall_length = 0
        while is_the_same_inclinations[tmp_wall_length]:
            tmp_wall_length += 1
            if tmp_wall_length >= len(is_the_same_inclinations):
                break
        if tmp_wall_length > 64:   # 22.5 degree
            walls.append((tmp_wall_start, tmp_wall_start + tmp_wall_length))
        tmp_wall_start += tmp_wall_length

    return walls


def calc_front_wall_for_servo(cartesian_array):
    walls = calc_front_wall(cartesian_array)
    if not walls:
        return 7500   # center

    max_wall_indexes = 0, 0
    for wall in walls:
        if diff(*max_wall_indexes) < diff(*wall):
            max_wall_indexes = wall
    # prepare face direction
    avg_urg_direction = average(max_wall_indexes)   # 0 - 512
    degree_direction = 180 * avg_urg_direction / 512.0 - 90   # -90 - 90
    servo_direction = -5000 * degree_direction / 180.0 + 7500   # 10000 - 5000

    # limit servo range
    if servo_direction > 8600:
        servo_direction = 8600
    elif servo_direction < 6400:
        servo_direction = 6400
    return servo_direction


def calc_front_wall_for_draw(cartesian_array):
    walls = calc_front_wall(cartesian_array)
    ret = []
    max_wall_indexes = 0, 0
    for wall in walls:
        if diff(*max_wall_indexes) < diff(*wall):
            max_wall_indexes = wall

    ret.extend(cartesian_array[max_wall_indexes[0]: max_wall_indexes[1]])
    ret.append((0,0))
    return ret


def compress_urg_data(urg_data):
    return zlib.compress(''.join(map(lambda d: pack('>H', d), urg_data)))


def decompress_urg_data(compressed):
    return unpack('>' + 'H' * 512, zlib.decompress(compressed))


def flatten(lists):
    ret = []
    for lst in lists:
        ret.extend(lst)
    return ret


def compass_diff(c1, c2):
    diff = ((c1 + 360 - c2) % 360)
    if diff > 180:
        diff = 360 - diff
    return diff
