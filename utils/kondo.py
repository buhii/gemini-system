# -*- coding: utf-8 -*-
from struct import pack


class FaceMotion(object):
    UP = 8305
    DOWN = 9301
    LEFT = 3205
    RIGHT = 10515


class RCB_4HV(object):
    def __init__(self, me):
        self.device = me.servo
        self.role = me.role

    def turn_normal(self):
        # turn face(yaw) to forward
        self.multi_servo(50, {0: 7500})

    def turn_face(self, direction):
        pass

    def uni_servo(self, ics, speed, position):
        # uni servo control
        ret = '\x07'   # size
        ret += '\x0f'  # command
        ret += chr(ics)
        ret += chr(speed)
        ret += pack('<h', position)
        checksum = chr(sum(map(ord, ret)) & 0xff)
        ret += checksum
        self.write(ret)

    def multi_servo(self, speed, ics_pos_dict):
        """
        move servos by same speed
        """
        # command
        ret = '\x10'
        # servo IDs
        for j in range(5):
            tmp = 0
            for i in range(8):
                if (j * 8 + i) in ics_pos_dict:
                    tmp += (1 << (i * 2))
            ret += chr(tmp)
        # speed
        if not (isinstance(speed, int) and speed < 256):
            raise ValueError("invalid settings: %s" % speed)
        ret += chr(speed)
        # positions
        ics_pos_pairs = sorted(ics_pos_dict.iteritems(), key=lambda tpl: tpl[0])
        for ics, pos in ics_pos_pairs:
            ret += pack('<h', pos)
        # size
        size = len(ret) + 2
        ret = chr(size) + ret
        # checksum
        checksum = chr(sum(map(ord, ret)) & 0xff)
        ret += checksum
        self.write(ret)

    def write(self, cmd):
        self.device.write(cmd)

    def read(self, length):
        return self.device.read(length)

    def multi_servo_diff_speed(self, ics_pos_speed_pairs):
        # move servos by different speeds
        pass
