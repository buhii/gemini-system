# -*- coding: utf-8 -*-
class AFSM(object):
    STOP = 0
    WANDER = 100
    LOADING = 2083
    LOADED = 2004
    FOUND_XBEE = 205
    TURN_TO = 29704
    VIBRATION_AVOID = 12566
    WAIT = 5902
    DETECT = 3672
    DIRECTION_L = 3095
    DIRECTION_R = 7059
    DIRECTION_F = 4073
    DIRECTION_B = 6062
