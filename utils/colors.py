# -*- coding: utf-8 -*-
BLACK = (0, 0, 0)
DARK_GRAY = (40, 40, 40)
GRAY = (90, 90, 90)
LIGHT_GRAY = (130, 130, 130)
WHITE = (255, 255, 255)
DARK_RED = (60, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
LIGHT_BLUE = (70, 70, 255)
