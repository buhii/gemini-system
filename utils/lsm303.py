# -*- coding: utf-8 -*-
from math import sqrt, atan2, degrees
from numpy import array
from struct import unpack
from utils.commons import RingBuffer, average, AccelerometerAverage

F_CPU = int(8.04 * 1000 * 1000)
g = 9.7976713

def ts2time(ts):
    # calculate timestamp(16bits) to time scale(in sec)
    return ts / (F_CPU / 64.0)


class LSM303DLH(object):
    DELIMITER_CODE = "\xff\x27\x95"
    # red's compass parameter
    mag_max = map(float, [294, 312, 647])
    mag_min = map(float, [-339, -332, -767])

    def __init__(self, serial_obj):
        self.device = serial_obj
        self.acc_avg_calc = AccelerometerAverage()
        self.old_timestamp = 0
        self.acc_diff = array([0] * 3)
        self.scale = 2.0
        self.head_buf = RingBuffer(boundary_size=4)
        self.moving_buf = RingBuffer(buf_size=4)

    def calc_average_start(self):
        self.acc_avg_calc.start()

    def calc_average_stop(self):
        self.acc_avg_calc.stop()
        self.acc_diff = self.acc_avg_calc.get_average()

    @property
    def is_calculating_average(self):
        return self.acc_avg_calc.is_running

    def get_acceleration(self):
        # return actual acceleration (m/sec^2)
        return self.acc / (2 ** 15) * self.scale * g

    def get_passed_time(self):
        # return delta time (sec)
        return float(self.timestamp_diff * 64) / F_CPU

    def is_delimiter(self):
        for char in self.DELIMITER_CODE:
            if self.device.read() != char:
                return False
        return True

    def wait_until_delimiter(self):
        try:
            while True:
                if self.is_delimiter():
                    break
        except KeyboardInterrupt:
            raise KeyboardInterrupt

    def read(self):
        self.wait_until_delimiter()
        self.acc_raw = array(unpack('<hhh', self.device.read(6)))
        self.mag_raw = array(unpack('>hhh', self.device.read(6)))
        self.gyr_tmp = unpack('>h', self.device.read(2))[0]
        self.temperature = (self.gyr_tmp + 13200) / 280.0 + 35  # see ITG-3200 specification note.
        self.gyr_raw = array(unpack('<hhh', self.device.read(6)))

        self.acc = self.acc_raw - self.acc_diff

        if self.is_calculating_average:
            self.acc_avg_calc.add(self.acc_raw)

        timestamp = unpack('<H', self.device.read(2))[0]
        self.timestamp_diff = timestamp - self.old_timestamp
        if self.timestamp_diff < 0:
            self.timestamp_diff += timestamp + 2 ** 16
        self.old_timestamp = timestamp

        self.calculate_rotate_matrix()

    def calculate_rotate_matrix(self):
        # See: http://d.hatena.ne.jp/NeoCat/20111022
        def euclid(ary):
            return sqrt(sum(map(lambda v: v * v, ary)))
        def resize(ary):
            return map(lambda v: v / 100.0, ary)
        def shift_and_scale(v, _min, _max):
            return (v - _min) / (_max - _min) * 2 - 1.0

        acc = resize(self.acc_raw)
        a = euclid(acc)

        #mag = resize(self.mag_raw)
        mag = map(lambda tpl: shift_and_scale(*tpl),
                  zip(self.mag_raw, self.mag_min, self.mag_max))
        m = euclid(mag)

        x, y, z = acc
        mx, my, mz = mag
        hx = my * z - mz * y
        hy = mz * x - mx * z
        hz = mx * y - my * x
        h = euclid((hx, hy, hz))

        rx = y * hz - z * hy
        ry = z * hx - x * hz
        rz = x * hy - y * hx
        r = euclid((rx, ry, rz))

        self.head_raw = (-degrees(atan2(hx / h, rx / r)) + 360 + 90) % 360   # 90 is device settings direction.
        self.filter_head()

        self.data_acc = (x / a * 20, y / a * 20, z / a * 20);
        self.data_mag = (mx / m * 20, my / m * 20, mz / m * 20);

        self.rotate_matrix = array([
            [ hx / h,  hy / h,  hz / h],
            [-rx / r, -ry / r, -rz / r],
            [  x / a,   y / a,   z / a],
            ])

    def filter_head(self):
        # append latest value and reduction noise with median filter.
        self.head_buf.append(self.head_raw)
        self.head_median = self.head_buf.median_filter()[-self.head_buf.boundary_size-1]   # boundary is not stable.
        self.moving_buf.append(self.head_median)
        self.head = self.moving_average()

    def moving_average(self):
        return average(self.moving_buf.buf)
