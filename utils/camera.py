# -*- coding: utf-8 -*-
"""
camera.py
  some useful stuff for Camera and Fern-based detector
"""
import os.path
import sys
import signal
import cv
import yaml
from PIL import Image
from datetime import datetime
from cStringIO import StringIO
from pyferns import planar_pattern_detector_wrapper as FD
from utils.detector import is_contour_convex
from gemini_system.universal import CAMERA_SIZE, LOCAL_OPERATION_DIR
from utils.devices import Me
from utils.afsm import AFSM
from record.models import CameraImage
from txosc import osc


REGION_THICKNESS = 20
BLACK = cv.RGB(0, 0, 0)


def draw_region(img, tpl, color):
    pt1, pt2, pt3, pt4 = zip(tpl[0::2], tpl[1::2])
    cv.Line(img, pt1, pt2, color, REGION_THICKNESS)
    cv.Line(img, pt2, pt3, color, REGION_THICKNESS)
    cv.Line(img, pt3, pt4, color, REGION_THICKNESS)
    cv.Line(img, pt4, pt1, color, REGION_THICKNESS)


def pk_to_doi_path(work_pk, figure_pk):
    return os.path.normpath(os.path.join(LOCAL_OPERATION_DIR, "%d-%d.doi" % (work_pk, figure_pk)))


def get_doi_files(work_pk, exhibition_pk):
    if not work_pk: return []
    # FIXME: code clone! @ museum.models
    yml_path = os.path.join(LOCAL_OPERATION_DIR, "exhibition_%d.yml" % exhibition_pk)
    works = yaml.load(open(yml_path).read())
    for work in works:
        if work['pk'] == work_pk:
            return map(lambda f_pk: pk_to_doi_path(work['pk'], f_pk), work['figures'])
    print Exception("Work model (pk=%s) does not exist!" % work_pk)
    #raise Exception("Work model (pk=%s) does not exist!" % work_pk)
    sys.exit()


def send_image(cv_im, results, is_record):
    """
    convert iplImage to JPEG image and send
    see: http://opencv.willowgarage.com/documentation/python/cookbook.html
    """
    pi = Image.fromstring("L", cv.GetSize(cv_im), cv_im.tostring())
    pi.thumbnail((256, 256))
    c = StringIO()
    pi.save(c, format="JPEG", quality=10)

    # あんまりよくないが録画中は送信する
    if is_record:
        msg = osc.Message('/img')
        msg.add(osc.BlobArgument(c.getvalue()))
        Me.send(Me.operator, msg)

    if is_record == True:
        # DB にも格納する
        ci = CameraImage(image=c.getvalue())
        ci.detect_area = results
        ci.save()


def force_exit(*args):
    print "force exit!"
    sys.exit()


# main AFSM
def gemini_camera(exhibition_pk, work_pk, shared_memory):
    """
    単一の AFSM として動作するようにする
    スレッドで動かすようにする
    """
    def info(text):
        Me.send_info(Me.operator, text)

    def send_detected_area(area):
        shared_memory['detected_area'] = area[:]
        msg = osc.Message('/detect')
        msg.add(str(','.join(map(str, area))))
        Me.send(Me.operator, msg)

    signal.signal(signal.SIGTERM, force_exit)

    img_sending_count = 0

    # 検出器構築開始
    shared_memory['AFSM_STATUS'] = AFSM.LOADING
    fds = []
    for doi_path in get_doi_files(work_pk, exhibition_pk):
        info("detector \'%s\' loading... @(%s)" % (doi_path, unicode(datetime.now())))
        fd = FD()
        try:
            fd.just_load(doi_path)  # very long time needed
        except KeyboardInterrupt:
            raise KeyboardInterrupt
        info("detector \'%s\' loaded. @(%s)" % (doi_path, unicode(datetime.now())))
        fds.append(fd)

    capture = cv.CreateCameraCapture(0)
    info("camera prepared.")
    result = cv.CreateImage(CAMERA_SIZE, cv.IPL_DEPTH_8U, 3)

    if len(fds) > 0:
        shared_memory['AFSM_STATUS'] = AFSM.LOADED
    else:
        shared_memory['AFSM_STATUS'] = AFSM.STOP   # no ferns detector, in short, initialize.

    while True:
        img = cv.QueryFrame(capture)
        gray = cv.CreateImage(CAMERA_SIZE, 8, 1)
        # Convert color input image to grayscale
        cv.CvtColor(img, gray, cv.CV_BGR2GRAY)

        # detect
        is_detected = False
        results = map(lambda fd: fd.detect(gray), fds)
        results = map(lambda area: is_contour_convex(area), results)

        for result in results:
            if result:
                draw_region(gray, result, BLACK)
                send_detected_area(result)
                is_detected = True

        # send detected images
        img_sending_count = (img_sending_count + 1) % 5
        if img_sending_count == 0 or is_detected:
            send_image(gray, results, shared_memory['is_record'])

    # if join, these statements aren't be executed.
    del capture
    del fd
    info("Camera AFSM disconnected.")
