# -*- coding: utf-8 -*-
"""
Settings for Device
"""
from uuid import getnode as get_mac
import serial
import socket
from utils.commons import insert_top_arg, TimeLimitedClosingSerial as TLCSerial
try:
    from twisted.internet import reactor
    from txosc import osc, dispatch, async, sync
except:
    pass


cast = {
    '0x68a86d0e48f0': 'OPERATOR',
    '0x40cced93ab2': 'OPERATOR',
    '0xdf09585dcL': 'GREEN',
    #'0x23ae61b5a1L': 'TEST',
    '0xdf0958661L': 'RED',
    '0x1c008abd1L': 'RED',
    '0x1c008a9d1L': 'GREEN',
}
ip_table = {
    #'TEST': '10.0.1.2',
    'OPERATOR': '10.0.1.6',
    'RED': '10.0.1.3',
    'GREEN': '10.0.1.4',
}
usb_config = {
    'OPERATOR': {
        #'compass': '/dev/tty.usbserial-A800ekmn',
        #'compass': '/dev/tty.usbserial-A800ekms',
        #'xbee': '',
        #'omniwheel': '/dev/ttyUSB0',
        },
    'RED': {
        'compass': '/dev/ttyUSB1',
        'servo': '/dev/ttyUSB0',
        'omniwheel': '/dev/ttyUSB2',
        'xbee': '/dev/ttyUSB3',
        },
    'GREEN': {
        'xbee': '/dev/ttyUSB0',
        'compass': '/dev/ttyUSB1',
        'omniwheel': '/dev/ttyUSB2',
        'servo': '/dev/ttyUSB3',
        },
    'TEST': {
        #'compass': 'COM14',
        #'compass': '/dev/tty.usbserial-A800ekmn',
        #'vsas': 'COM1',
        #'vsas': '/dev/tty.usbserial',
        },
}
device_config = {
    'compass': {
        'parity': serial.PARITY_NONE,
        'baudrate': 9600
        },
    'vsas': {
        'parity': serial.PARITY_NONE,
        'baudrate': 115200,
        },
    'xbee': {
        'baudrate': 9600
        },
    'urg': {},   # pyliburg set parameters.
    'omniwheel': {
        'parity': serial.PARITY_EVEN,
        'baudrate': 38400,
        },
    'servo': {
        'parity': serial.PARITY_EVEN,
        'baudrate': 115200,   # 1250000
        },
}


class Performer(object):
    def __init__(self, mac):
        self.mac = mac
        try:
            self.role = cast[self.mac]
        except KeyError:
            raise IOError("Cannot get your role: '%s'" % my_mac)
        self.ip = ip_table[self.role]

        print "Hello, %s(%s)." % (self.role, self.ip)

        if self.role == 'OPERATOR':
            self.set_operator()
        elif self.role == 'GREEN' or self.role == 'RED':
            self.set_gemini()

        self.set_usb()
        self.osc_port = 59603
        self.set_server()
        self.set_client()

    def send(self, addresses, msg):
        # insert role to message
        msg = insert_top_arg(msg, self.role)
        if not (isinstance(addresses, list) or isinstance(addresses, tuple)):
            addresses = [addresses]
        for addr in addresses:
            if not self.ip2client[addr]:
                self.set_client()   # resetting client
            try:
                self.ip2client[addr].send(msg)
            except KeyError:
                print "[ERROR] KeyError %s in %s" % (addr, self.ip2client)
                self.set_client()   # resetting client
            except Exception as e:
                print "[ERROR] cannot send message (%s) %s:%s" % (unicode(e), addr, self.osc_port)
                self.set_client()   # resetting client
                return False
            except KeyboardInterrupt:
                raise KeyboardInterrupt
        return True

    def send_info(self, who, info):
        msg = osc.Message('/info')
        msg.add(self.role)
        msg.add(str(info))
        self.send(who, msg)

    def send_action_end(self, reliable_stuff):
        msg = osc.Message('/action_end')
        msg.add(reliable_stuff)
        result = Me.send(Me.operator, msg)

    def add_osc_listener(self, address, callback):
        self.receiver.addCallback(address, callback)

    def listenTCP(self):
        self._server_port = reactor.listenTCP(self.osc_port, async.ServerFactory(self.receiver))

    def set_operator(self):
        self.children = map(lambda r: ip_table[r], ('RED', 'GREEN'))
        self.red = ip_table['RED']
        self.green = ip_table['GREEN']

    def set_gemini(self):
        brother = 'RED' if self.role == 'GREEN' else 'GREEN'
        self.brother = ip_table[brother]
        self.operator = ip_table['OPERATOR']
        if self.role == 'RED':
            self.brother_xbee_id = '0x1234'   # GREEN's xbee id
        else:
            self.brother_xbee_id = '0x1000'   # RED's xbee id

    def set_usb(self):
        usb_dict = usb_config[self.role]
        for attr, port in usb_dict.iteritems():
            try:
                print "%9s: %s, %s" % (attr, port, device_config[attr])
                #serial_obj = serial.Serial(port, **device_config[attr])
                serial_obj = TLCSerial(port, **device_config[attr])
            except serial.serialutil.SerialException as e:
                print "[WARNING] Cannot open %s (%s)" % (port, e)
                serial_obj = None
            setattr(self, attr, serial_obj)

    def set_server(self):
        """
        prepare OSCServer
        """
        self.receiver = dispatch.Receiver()
        self.receiver.setFallback(self.fallback)

    def fallback(self, message, address):
        print "got %s from %s" % (message, address)

    def _errback(self, reason):
        print("An error occurred: %s" % (reason.getErrorMessage()))

    def set_client(self):
        if self.role == 'TEST':
            return

        if not hasattr(self, 'ip2client'):
            self.ip2client = {}
        for other in (set(['RED', 'GREEN', 'OPERATOR'])):
            ip_other = ip_table[other]
            self.ip2client[ip_other] = None
            try:
                self.ip2client[ip_other] = sync.TcpSender(ip_other, self.osc_port)
            except socket.error, e:
                print(str(e))

    def reactor_run(self):
        # every frame, We listen request.
        print "[INFO] run reactor!"
        reactor.run()

    def force_stop(self):
        if self.role in ("GREEN", "RED"):
            from utils.omniwheel import omniwheel_stop
            omniwheel_stop(self, True)

    def close(self):
        self.force_stop()

    def __eq__(self, role):
        return self.role == role


my_mac = hex(get_mac())
Me = Performer(my_mac)
